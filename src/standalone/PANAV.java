package standalone;

import java.io.*;
import java.util.regex.*;


public class PANAV {

  public static final double allow = 0.1;
  public static final int RES_LEN_MIN = 3;
  public static final int RES_LEN_MAX = 6;

  Object[][] calibrated;

  File returnfile;
  String line="";

  CSFileReader strreader;

  private String get_validation_mesg() {
    String dmesg = "";

    dmesg += returnfile.getName() + "\n";
    String seq = strreader.getSequence();
    int[] aaIndex = strreader.getSequenceLine();
    Double[][] sixtycal = strreader.getSixtyCal();

    for (int r = PANAV.RES_LEN_MIN; r <= PANAV.RES_LEN_MAX; r++) {

      boolean[] CaCb_sel = strreader.getscannableList(r);
      Scanner_3 scan = new Scanner_3(seq, aaIndex, sixtycal, r,
                                     allow, CaCb_sel);
      int[] s_position = scan.get_s_pos();
      String[] score = scan.get_Score();
      String[] s_nor_prob = scan.get_o_prob_nor();
      dmesg+= "\n" + r + "-Residue Scan: \n\n";
      boolean[] x = new boolean[s_position.length + r];
      for (int q = 0; q < x.length; q++) {
          x[q] = true;
      }

      for (int q = 0; q < x.length - r; q++) {
          for (int m = 0; m < r; m++) {
          if (s_position[q] > 0) {
            x[q + m] = false;
          }
        }
      }
      for (int i = 0; i < x.length-1; i++) {
        if (x[i]) {
          dmesg+= seq.substring(i, i + 1);
        }
        else {
          dmesg+=seq.substring(i, i + 1);
          //dmesg+="<b>"+seq.substring(i, i + 1)+"</b>"; //If want this part stands out(bold)
        }

        if (Integer.toString(i + 1).endsWith("0") && i > 0) {
          dmesg+= "  ";
        }
      }
      dmesg+="\n\nOriginal Assignment and Probability vs Suggested Assignment and Probability \n\n";
      dmesg+=scan.get_message()+"\n";
      dmesg+= "# of selected frags: " + score[0] + ";   # of confirmed frags: " +
             score[1] +"; CONA Score:  " + score[2] + "\n";

      line=line+score[2]+" ";
    }

    return dmesg;
  }


  public boolean processFolder (File returnfile) {
    boolean status=true;
    try {
      
      if  (returnfile!=null && returnfile.isDirectory()) {
      
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File directory, String fileName) {
                return fileName.endsWith(".str") || fileName.endsWith(".nef");
            }
        };
        File files[]=returnfile.listFiles(filter);
      
        for (int i=0;i<files.length;i++) {
          File file=files[i];
          String path=file.getParent();
          System.out.println("Processing file: " + file.getName());
          String fileAbsPath=file.getAbsolutePath();
          String outFile = fileAbsPath.replaceAll("\\.(?:str|nef)$", ".out");
        
          String fileType = "";
          if (fileAbsPath.endsWith(".str")) {
            fileType = "str";
          } else if (fileAbsPath.endsWith(".nef")) {
            fileType = "nef";
          }
        
          boolean success = processFile(fileType,fileAbsPath, outFile);
          if (!success) {
            status=false;
          
          }
        
        }
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return status;
  }


  public boolean processFile(String type, String in, String out) {
    
    boolean success = true;
    try {
      returnfile = new File(in);
      strreader = new CSFileReader(returnfile, type);
      calibrated = strreader.getCal_DisplayElements();
      String ref_mesg="";
      if (strreader.need_ref_cal) {
        ref_mesg="Detected reference offsets \n" +
                       strreader.ref_shift +
                       "\n\n" + strreader.get_D_S_shift();
      }
      else {
        ref_mesg="no reference offsets required" + "\n\n" +
                       strreader.get_D_S_shift();
      }
      write_calib_seq(calibrated, out);
      String validateout=get_validation_mesg();
    
      FileWriter outfile = new FileWriter(out);
      outfile.write(validateout +"\n\n"+ ref_mesg);
      outfile.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      success=false;
    }
    
    return success;
  }


    /**
     * write_calib_seq
     *
     * @param calibrated Object[][]
     */
  private void write_calib_seq(Object[][] cal, String out) {
    String[] headings = new String[] {"Seq","C","CA","CB","N", "H","HA", "B-prob","C-prob","H-prob", "SecStr"};
    String s=String.format("%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\n",(Object[])headings);
    try {
      FileWriter outfile = new FileWriter(out+"_calibrated");
      outfile.write(s);
      for(int i=0;i<cal.length;i++){
        outfile.write(String.format("%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t%6s\n", cal[i]));
      }
      outfile.close();
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }


  public PANAV() {

  }

///////////////////////////////////////////////////


  public static void main(String[] args) {
    
    if (args.length == 1) {
      
      if (args[0].equals("-h") || args[0].equals("--help")) {
        System.out.println("Usage: java -jar PANAV.jar [<folder> | <format> <file>]");
        System.out.println("  No arguments: Run GUI.");
        System.out.println("  One argument: Process .str and .nef files in given folder.");
        System.out.println("  Two arguments: Provide file format and name of chemical shift file.");
        System.out.println("    <format> may be one of: str, nef, shiftX_table, or shiftY.");
        System.exit(0);
      } else {

        try {
          PANAV ha = new PANAV ();
          File folder = new File(args[0]);
          boolean success=ha.processFolder (folder);
          if (!success) {
            System.out.println("Failed to process .str files in folder");
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }
    else if (args.length == 2) {

      String fileType = args[0];
      String filename = args[1];

      try {

        System.out.println("Processing file " + filename + " with format " + fileType + "...");

        File file = new File(filename);
        String fileAbsPath = file.getAbsolutePath();
        String outFile;
        if (Pattern.matches("\\.(?:str|nef|shiftx|shifty)$", fileAbsPath)) {
          outFile = fileAbsPath.replaceAll("\\.(?:str|nef|shiftx|shifty)$", ".out");
        } else {
          outFile = fileAbsPath + ".out";
        }

        PANAV ha = new PANAV ();
        boolean success = ha.processFile(fileType, fileAbsPath, outFile);

        if (success) {
          System.out.println("Results written to " + outFile);
        }
        else {
          System.out.println("Failed to process file " + file.getName());
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }

    }
    else {
      PANAV ha = new PANAV ();
      PANAVGUI pg = new PANAVGUI (ha);
      pg.createGUI();
    }
  }

}
