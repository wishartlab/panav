package standalone;

// 7/4/09 bmr6730 , CO large difference, remove abnormal assignment during calibration.
// 
// for cys, just use natural occurrance only
//Gly no CB, proline no N just use natural occurrance only
import java.text.NumberFormat;
import java.util.Vector;

public class CSI_Sec { 
 
 ReSpace_2 rsp = new ReSpace_2();
 CSI_AVE_STD csid = new CSI_AVE_STD();
 statist std=new statist(2.5);

 String[][] seqSec_1;  // no self_cali  seq+num, prob_b, prob_Coil, prob_helx, sec str,sec str
                                       // 0         1      2           3         4       5 (for manual smooth)
 String[][] seqSec_2; // self_cali

 String[][] seq_C_CSI;// calibrated CSI, for needed nuclei only 7-4-09

 
 double[] csi_off = {0,0,0,0};// Cb manually add 0.4 ppm, 12-28-09

 double[] need_cal_cut ={0.0,0.0,0.0, 0.0};  // 7_4_09 cut_off for definition of CO, CA, CB, and N
 
// double[] need_cal_cut ={1.0,1.0,1.0, 1.5};  // 7_4_09 cut_off for definition of CO, CA, CB, and N

 
 boolean yn_HA = true;
 int[] csi_num={0,0,0,0};//number of chemical shifts of CO, CA, CB, and N
 int num_cutoff=20;

 int it_num=2;

  String Typo_err= "Identified Deviant Shifts:\n";

  


// constructor input   seqCsi from bmr_file_read or strRead_2,  0 Num, 1 res, 2 pre, 3 CO, 4 CA, 5 CB, 6 N, 7 HN, 8 HA 

public CSI_Sec (String[][] seqCsi) {
 

     get_csiOff(seqCsi);

     seqSec_1 = new String[seqCsi.length][6];

     for (int g=0;g<seqCsi.length;g++) {  
      //corrected 7-4-09 seqCsi.length
         for (int s=3;s<seqCsi[0].length-2;s++)  {
          try {Double.parseDouble(seqCsi[g][s]);
               csi_num[s-3]++;
              } catch   (Exception egg) {};
       };
     };
  
     seq_C_CSI= new String[seqCsi.length][seqCsi[0].length];
     for (int a=0; a<seqCsi.length; a++) {
          for (int b=0; b<seqCsi[0].length; b++)
              seq_C_CSI[a][b]=seqCsi[a][b];
     };



     for (int i=0;i<seqCsi.length;i++) {
         String[] se = csid.get_ResProb(getRow(seqCsi,i));  
        
         for (int m=0;m<se.length;m++)   {
             seqSec_1[i][m]=se[m];
             //test +="  "+ seqSec_1[i][m];
         };    
     };
     
 
     seqSec_2 = new String[seqCsi.length][6];
     get_csiOff(seqCsi);

     for (int i=0;i<seqCsi.length;i++) {
         String[] se = csid.get_ResProb(get_Cal_Row(seqCsi, i));

         for (int m=0;m<se.length;m++)
             seqSec_2[i][m]=se[m];
     };
  
    
};


  
public String get_Mes(boolean choose) {

    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    nf.setMinimumFractionDigits(2);
    String message="";

    String[] n_off = {"CO","CA","CB","N"};


    if (choose) {
        message +="Following CS ref offsets were detected and used during the prediction\n"; 
        for (int i=0;i<csi_off.length;i++) 
            message += n_off[i]+":  "+ nf.format(csi_off[i]) +"  ";
          
    }
    else {
        message +="Following CS reference offsets were detected\n";     
       
       String tmp="";
       for (int i=0;i<csi_off.length;i++)  {
           message += n_off[i]+":  "+ nf.format(csi_off[i]) +"  ";
           if(csi_off[i]>need_cal_cut[i]||(-csi_off[i])>need_cal_cut[i])  
              tmp +=n_off[i]+"  ";
       };

       if(tmp.length()>0)
       
       message+= "\nCS ref calibrations are recommended for: " +tmp;
   };

    return
      message;

};
 
// Feb 9, 2007   
public String get_ref_calib () { 

    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    nf.setMinimumFractionDigits(2);
    String message="";

    String[] n_off = {"CO","CA","CB","N"};
    for (int i=0;i<csi_off.length;i++)  {
        message += n_off[i]+": ";
        double csiOff=csi_off[i];
        if (i==2 && csi_num[2]<10) {
        	csiOff=0.0;
        }
        if(csi_off[i]>need_cal_cut[i]||(-csi_off[i])>=need_cal_cut[i]) 
          message+= nf.format(csiOff) +"ppm     ";
         else  
           message+= "0.00 ppm     ";     
       };
 
 
    return
      message;

};
 


//modified Oct 27, 2004 
///////////////////// 
protected void  get_csiOff (String[][] sCSI) {
  
     NumberFormat nf = NumberFormat.getInstance();
     nf.setMaximumFractionDigits(2);
     nf.setMinimumFractionDigits(2);
   

     for(int o=0;o<csi_off.length;o++) {
        csi_off[o] = 0.0;
       // if (o==2)
       // csi_off[o]=0.4; // add 0.4 ppm to CB, 12-28-09


     }



     String[] n_off = {"CO","CA","CB","N"};
 
     Vector v_off [] = new  Vector [4]  ;  
          for(int v=0;v<v_off.length;v++)
              v_off[v]=new Vector<String>();
 
     int num_HA=0;
     for(int h=0;h<sCSI.length;h++) {
       try {Double.parseDouble(sCSI[h][8]); 
            num_HA++;
           } catch (Exception ed) {}
     };
     if(num_HA>20)
       yn_HA=true;
     else
       yn_HA=false;
  
    
     for (int i=0;i<sCSI.length;i++) {
       
           String res=sCSI[i][1];
           
          String[] sec_ha;
          if(yn_HA)
           sec_ha = csid.get_Res_Nuc_Prob(res, "", "HA",sCSI[i][8]);
          else {
               String[] a=  get_Cal_Row (sCSI, i);  
               sec_ha = csid.get_ResProb(a);
          };

         for (int n=0;n<n_off.length; n++)  {
             String nu = n_off[n];
                      
               try {double csi_obs  = Double.parseDouble(sCSI[i][3+n]);
                    double[] csi_av;
                    if(!nu.equals("N"))
                          csi_av=csid.get_ave_std(nu,res,sec_ha[4]);
                    else
                          csi_av =csid.getN_ave_std(res,sCSI[i][2],sec_ha[4]);

                    boolean flg=false;
                    if( (res.equals("C") ||res.equals("G")) && nu.equals("CB") )  {
                            flg=true;

                   // System.out.println(res+" " + nu + " " + sCSI[i][3+n]);

                    };
                    if (res.equals("P") && nu.equals("N"))
                            flg=true;
                    if(!flg )
                            v_off[n].add (Double.toString(csi_av[0]- csi_obs));
                     
                    } catch (Exception e3) {};


             };//eof n

     //  System.out.println("CO  "+ Integer.toString(v_off[0].size()) + " "+Integer.toString(v_off[1].size()) );



     }; //eof for

   // System.out.println("CO  "+ Integer.toString(v_off[0].size()) + " "+Integer.toString(v_off[1].size())+ 
   //                             " "+Integer.toString(v_off[2].size())  +
    //                             " "+Integer.toString(v_off[3].size())  
    //            );


           
     for (int g=0;g<v_off.length;g++)    
          csi_off[g] += Double.parseDouble(std.StatAna(v_off[g])[0]); 
  
         
/////////////////////
 
    for (int r=0;r<it_num;r++) {
        for(int v=0;v<v_off.length;v++)
              v_off[v]=new Vector();

        for (int i=0;i<sCSI.length;i++) {
              String res=sCSI[i][1];

          String[] a=  get_Cal_Row (sCSI, i);  
          String[] sec_ha = csid.get_ResProb(a);

          for (int n=0;n<n_off.length; n++)  {
               String nu = n_off[n];
               try { double csi_obs  = Double.parseDouble(a[3+n]);
                     double[] csi_av;
                     if(!nu.equals("N"))
                          csi_av=csid.get_ave_std(nu,res,sec_ha[4]);
                     else
                          csi_av =csid.getN_ave_std(res,sCSI[i][2],sec_ha[4]);
                     boolean flg=false;
                     if( (res.equals("C") ||res.equals("G")) && nu.equals("CB") ) 
                               flg=true;
                     if (res.equals("P") && nu.equals("N"))
                               flg=true;
                     if(!flg)
                         v_off[n].add (Double.toString(csi_av[0]- csi_obs));
                     
                   } catch (Exception e3) {};
                 };//eof n            
           }; //eof fori
           
           for (int g=0;g<v_off.length;g++)    
             csi_off[g] += Double.parseDouble(std.StatAna(v_off[g])[0]); 

             csi_off[2] +=0.4;//12-28-09 add 0.4 ppm to CB
 
   
     
  };//eof r
 

};   
  
//////////////////////////////////////////////////////////////////////////////////// csi_off....
  
// modified 7/4/09 consider big mis referenced csi eg 6730 for CO, 4150 and 5179 for CB
 
 
          
////////////////////////////

protected String[] getRow (String[][] str, int i) {

  String [] tmp = new String[str[0].length];
  for(int j=0; j<tmp.length;j++)
     tmp[j]=str[i][j];

  return tmp;
};
  

protected String[] get_Cal_Row (String[][] a, int i) {
     NumberFormat nf = NumberFormat.getInstance();
     nf.setMaximumFractionDigits(2);
     nf.setMinimumFractionDigits(2);
 
  

  String [] tmp = new String[a[0].length];

  for(int j=0;j<tmp.length;j++)
       tmp[j]=a[i][j];

  try { tmp[3] = nf.format(Double.parseDouble(a[i][3])+csi_off[0]); } catch (Exception f3) {tmp[3]=a[i][3];};
  try { tmp[4] = nf.format(Double.parseDouble(a[i][4])+csi_off[1]); } catch (Exception f4) {tmp[4]=a[i][4];};
  try { tmp[5] = nf.format(Double.parseDouble(a[i][5])+csi_off[2]); } catch (Exception f5) {tmp[5]=a[i][5];};
  try { tmp[6] = nf.format(Double.parseDouble(a[i][6])+csi_off[3]); } catch (Exception f6) {tmp[6]=a[i][6];};

  return tmp;
};



public String[][] get_Sec(boolean yn) {
 if(yn)
  return seqSec_2
;
 else
  return seqSec_1;
};

public String[][] get_Sec_No_Cal() {
  return seqSec_1;
};

public String[][] get_Sec_with_Cal() {
  return seqSec_2;
};



public String get_ynHA() {
   if(yn_HA)
     return "Yes";
   else
     return "No";
};


public String[][] get_ref_seqCSI() {
     NumberFormat nf = NumberFormat.getInstance();
     nf.setMaximumFractionDigits(2);
     nf.setMinimumFractionDigits(2);

//modified july 4 2009

  
     int a= seq_C_CSI.length, b=seq_C_CSI[0].length;
     String[][] tm=new String[a][b];

     for (int h=0; h<a; h++) {
        for (int y=0; y<b; y++)  
           tm[h][y]=seq_C_CSI[h][y];
     };

     double[] tmp = {0.0,  0.0,  0.0,  0.0};  

     for(int i=0; i<tmp.length;i++) {
        if(csi_num[i]>num_cutoff && (csi_off[i]>need_cal_cut[i]||(-csi_off[i])>=need_cal_cut[i]))
          tmp[i]= csi_off[i];
     };
  
// 0 Num, 1 res, 2 pre, 3 CO, 4 CA, 5 CB, 6 N, 7 HN, 8 HA    
     for (int g=0; g<a; g++) {
        for (int f=3; f<b-2; f++) {
  
         try {
                 if(csi_off[f-3]>=need_cal_cut[f-3]||(-csi_off[f-3])>=need_cal_cut[f-3])
                  tm[g][f] = nf.format(Double.parseDouble(seq_C_CSI[g][f])+csi_off[f-3]); 
               } catch (Exception g3) {};

       };
     
     };


    return tm;
};


public boolean need_cal () {
 boolean bl=false;
     for(int i=0; i<csi_off.length;i++) {
        if(csi_num[i]>num_cutoff &&  (csi_off[i]>need_cal_cut[i]||(-csi_off[i])>=need_cal_cut[i]))
          bl=true;
     };
 return bl;

};


public String[] get_ref_off() {
     NumberFormat nf = NumberFormat.getInstance();
     nf.setMaximumFractionDigits(2);
     nf.setMinimumFractionDigits(2);
 
     String[] tmp = new String[4];
     for(int i=0; i<tmp.length;i++) {
        if(csi_num[i]>num_cutoff)
          tmp[i]= nf.format(csi_off[i]);
        else
          tmp[i]= "NA";           
     };   
    return tmp;
};


 
    
} ///~



 
