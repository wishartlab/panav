package standalone;

import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;
import java.util.regex.*;


public class PANAVGUI
    extends JFrame implements ActionListener {

  private static final long serialVersionUID = 1L; // added 10_21_2009

  private PANAV panav;

  protected JSplitPane m_sp;
  JTextArea Ref_mess = null;
  JTextPane Val_mess = null;
  JScrollPane Ref_jp = null;
  JScrollPane Val_jp = null;

  GridBagConstraints constraints = new GridBagConstraints();

  chemicalShift chemical;
  JTable table;
  JScrollPane scroll;
  chemicalShift chemicalcal;
  JTable tablecal;
  JScrollPane scrollcal;

  JTabbedPane tables = new JTabbedPane();
  Border forTables = BorderFactory.createEtchedBorder();

  Object[][] atFirst;
  Object[][] calibrated;

  File returnfile;

  CSFileReader strreader;
  private static final String MENULABEL_FOLDER = "Read .str and/or .nef in Folder";
  private static final String MENULABEL_NMRSTAR = "Read .str File";
  private static final String MENULABEL_NEF = "Read .nef File";
  private static final String MENULABEL_SHIFTX = "Read SHIFTX File";
  private static final String MENULABEL_SHIFTY = "Read SHIFTY File";

  public File loadStrFile(int mode) {
    File file = null;
    try {
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(mode);
    int result = chooser.showOpenDialog(this);
    if (result != JFileChooser.CANCEL_OPTION) {
      
      file = chooser.getSelectedFile();
    }
    }
    catch (Exception e) {
      System.out.println("Problem reading file/folder");
    }
    return file;
  }


  public void actionPerformed(ActionEvent e) {

    String command = e.getActionCommand();
    //System.out.println("command="+command);
    if (command.equals(MENULABEL_FOLDER)) {
      
      returnfile=loadStrFile(JFileChooser.DIRECTORIES_ONLY);
      
      
        this.panav.processFolder(returnfile);      
      
    }
    
    // loadStrFile();// to load  file

    else if (command.equals(MENULABEL_NMRSTAR) || command.equals(MENULABEL_NEF) ||
        command.equals(MENULABEL_SHIFTX) || command.equals(MENULABEL_SHIFTY)) {

      returnfile=loadStrFile(JFileChooser.FILES_ONLY);

      if (command.equals(MENULABEL_NMRSTAR)) {
        strreader = new CSFileReader(returnfile, "str");
      }
      else if (command.equals(MENULABEL_NEF)) {
        strreader = new CSFileReader(returnfile, "nef");
      }
      else if (command.equals(MENULABEL_SHIFTX)) {
        strreader = new CSFileReader(returnfile, "shiftX_table");
        // System.out.println("shift_X");
      }
      else if (command.equals(MENULABEL_SHIFTY)) {
        strreader = new CSFileReader(returnfile, "shiftY");
        // System.out.println("shift_Y");
      }
      ;

      atFirst = strreader.getDisplayElements();
      addTable();

      calibrated = strreader.getCal_DisplayElements();

      if (strreader.need_ref_cal) {
        addCalTable();
        Ref_mess.setText("Detected reference offsets \n" + strreader.ref_shift +
                         "\n\n" + strreader.get_D_S_shift());
      }
      else {
        Ref_mess.setText("no reference offsets required" + "\n\n" +
                         strreader.get_D_S_shift());
      }

      add_Val();

    }
   //eof command
    else if (command.equals("Save Output")) {

      try {
        FileWriter out = new FileWriter("../" + returnfile.getName() +
                                        "_output.txt");
        try {
          Document c = Val_mess.getDocument();
          out.write(c.getText(0, c.getLength()) + Ref_mess.getText());
        }
        catch (BadLocationException ble) {}

        out.close();

      }
      catch (IOException ex) {
        ex.printStackTrace();
      }
      JOptionPane.showMessageDialog( this,
                                     "Output saved as "+"../" + returnfile.getName() +
                                        "_output.txt","File Saved!",
                                JOptionPane.INFORMATION_MESSAGE);

    }
    else if (command.equals("Exit")) {
      
      setVisible(false); //you can't see me!
      dispose();
    }
    else if (command.equals("About")) {
      
      JOptionPane.showMessageDialog(this, "Version 2.2.2. Last update May 4, 2016");
    }
  }

///


  void addGB(Component component, int x, int y, double height, double width) {
    constraints.gridx = x;
    constraints.gridy = y;
    constraints.weightx = height;
    constraints.weighty = width;
    add(component, constraints);
  }

///

  public void addTable() {
    chemical = new chemicalShift(atFirst);
    table = new JTable(chemical);
    scroll = new JScrollPane(table);
    tables.addTab(returnfile.getName(), scroll);

    table.repaint();

  }


  public void addCalTable() {
    chemicalcal = new chemicalShift(calibrated);
    tablecal = new JTable(chemicalcal);
    scrollcal = new JScrollPane(tablecal);
    tables.addTab("No_deviant_Ref_calibrated", scrollcal);
    table.repaint();
  }


  public void add_Val() {

    SimpleAttributeSet red = new SimpleAttributeSet();
    StyleConstants.setForeground(red, Color.red);
    StyleConstants.setBold(red, true);
    SimpleAttributeSet blue = new SimpleAttributeSet();
    StyleConstants.setForeground(blue, Color.blue);
    SimpleAttributeSet italic = new SimpleAttributeSet();
    StyleConstants.setItalic(italic, true);
    StyleConstants.setForeground(italic, Color.orange);

    Val_mess.setFont(new Font("Serif", Font.PLAIN, 24));

    Document d = Val_mess.getDocument();

    try {
      d.remove(0, d.getLength());
      d.insertString(d.getLength(), returnfile.getName() + "\n", blue);
    }
    catch (BadLocationException ble1) {}
    ;

    Val_mess.setFont(new Font("Serif", Font.PLAIN, 12));

    String seq = strreader.getSequence();
    int[] aaIndex = strreader.getSequenceLine();
    Double[][] sixtycal = strreader.getSixtyCal();

    for (int r = PANAV.RES_LEN_MIN; r <= PANAV.RES_LEN_MAX; r++) {

      boolean[] CaCb_sel = strreader.getscannableList(r);
      Scanner_3 scan = new Scanner_3(seq, aaIndex, sixtycal, r,
                                     PANAV.allow, CaCb_sel);

      int[] s_position = scan.get_s_pos();

      String[] score = scan.get_Score();
      String[] s_nor_prob = scan.get_o_prob_nor();

      try {
        Val_mess.setFont(new Font("Serif", Font.PLAIN, 24));

        d.insertString(d.getLength(),
                       "\n" + Integer.toString(r) +
                       "-Resdue Scan: \n\n",
                       blue);
        Val_mess.setFont(new Font("Serif", Font.PLAIN, 12));

        boolean[] x = new boolean[s_position.length + r];
        for (int q = 0; q < x.length; q++) {
          x[q] = true;
        }

        for (int q = 0; q < x.length - r; q++) {
          for (int m = 0; m < r; m++) {
            if (s_position[q] > 0) {
              x[q + m] = false;
            }
          }
        }
        ;

        for (int i = 0; i < x.length; i++) {

          try {
            if (x[i]) {
              d.insertString(d.getLength(), seq.substring(i, i + 1), blue);
            }
            else {
              d.insertString(d.getLength(), seq.substring(i, i + 1), red);
            }

            if (Integer.toString(i + 1).endsWith("0") && i > 0) {
              d.insertString(d.getLength(), "  ", blue);
            }
          }
          catch (Exception bdle) {}
          ;

          } ; // eof i

          d.insertString(d.getLength(), "\n\nOriginal Assignment and Probability vs Suggested Assignment and Probability \n\n",
                         blue);

          d.insertString(d.getLength(), scan.get_message(), blue);
          d.insertString(d.getLength(), "\n", blue);

          d.insertString(d.getLength(),
                         "# of selected frags: " + score[0] +
                         ";   # of confirmed frags: " +
                         score[1] +
                         "; CONA Score:  " + score[2] + "\n", blue);
          
        }
        catch (Exception ble) {}

      }
      ; //eof r

    }
    // eof Val


  public PANAVGUI(PANAV panav) {
    this.panav = panav;
  }


  public void createGUI () {

    
    this.Ref_mess=new JTextArea();
    Val_mess = new JTextPane();
    Ref_jp = new JScrollPane(Ref_mess);
    Val_jp = new JScrollPane(Val_mess);
    tables.setBorder(BorderFactory.createTitledBorder(forTables,
        "CSI and Sec Structure", TitledBorder.LEFT, TitledBorder.TOP));

    setLayout(new GridBagLayout());
    constraints.weightx = 1;
    constraints.weighty = 1;
    constraints.fill = GridBagConstraints.BOTH;

   
    JPanel R_JP = new JPanel();
    R_JP.setLayout(new BoxLayout(R_JP, BoxLayout.X_AXIS));
    R_JP.setBorder(new TitledBorder(new EtchedBorder(50),
                                    "Reference offsets and deviant shifts"));
    R_JP.add(Ref_jp);
    
    

    JPanel V_JP = new JPanel();
    V_JP.setLayout(new BoxLayout(V_JP, BoxLayout.X_AXIS));
    V_JP.setBorder(new TitledBorder(new EtchedBorder(50), "Validation"));
    V_JP.add(Val_jp);

    m_sp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, tables, V_JP);
    m_sp.setDividerLocation(150);
    m_sp.setDividerSize(8);

    m_sp.setContinuousLayout(false);
    m_sp.setOneTouchExpandable(true);

    addGB(m_sp, 0, 0, 1, 16);
    addGB(R_JP, 0, 1, 1, 4);
    
    JMenuBar menu = new JMenuBar();
    JMenu filelist = new JMenu("File");
    JMenu panav=new JMenu("PANAV");
    
    JMenuItem folder = new JMenuItem(MENULABEL_FOLDER);
    
    JMenuItem open = new JMenuItem(MENULABEL_NMRSTAR);
    JMenuItem open2 = new JMenuItem(MENULABEL_NEF);
    JMenuItem open3 = new JMenuItem(MENULABEL_SHIFTX);
    JMenuItem open4 = new JMenuItem(MENULABEL_SHIFTY);

    JMenuItem save = new JMenuItem("Save Output");
    
    
    
    JMenuItem about = new JMenuItem("About");
    JMenuItem exit = new JMenuItem("Exit");
    panav.add(about);
    panav.addSeparator();
    panav.add(exit);

    menu.add(filelist);
    menu.add(panav);
    filelist.add(folder);
    filelist.addSeparator();
  
    filelist.add(open);
    filelist.add(open2);
    filelist.add(open3);
    filelist.add(open4);

    filelist.add(save);
    
    
    folder.addActionListener(this);
    open.addActionListener(this);
    open2.addActionListener(this);
    open3.addActionListener(this);
    open4.addActionListener(this);
    save.addActionListener(this);
    
    about.addActionListener(this);
    exit.addActionListener(this);
    
    setJMenuBar(menu);
    
    
    this.setVisible(true);
    this.setSize(900, 600);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

}
