package standalone;

//mofifed 10_18_2009, switch move N from last to before HN

 


public class Gaussian {


//--Average Readings and Standard Deviations-------------------------------------------------------------------

         double[][][] CSIDIS = { 

	 {
     	 { 175.24,   1.62,   177.38,   1.64,   179.33,   1.58	},
      	 { 173.6,    1.71,   174.8,    1.49,   176.89,   1.12	},
     	 { 175.14,   1.59,   176.01,   1.49,   177.47,   1.86	},
      	 { 175.0,    1.27,   175.98,   1.5,    178.48,   1.34	},
      	 { 174.02,   2.01,   174.98,   1.94,   176.25,   1.86	},
      	 { 173.1,    2.69,   174.21,   1.85,   176.51,   1.7	},
      	 { 173.76,   2.26,   174.85,   1.77,   176.69,   1.28	},//C
      	 { 174.8,    1.4,    175.5,    1.6,    176.82,   1.79	},
      	 { 174.82,   1.16,   175.94,   1.42,   177.48,   2.48	},
      	 { 175.14,   1.29,   176.47,   1.72,   178.22,   1.88	},
      	 { 174.63,   1.45,   175.73,   1.59,   177.71,   1.92	},
      	 { 174.54,   1.27,   175.02,   1.43,   176.26,   1.57	},
      	 { 176.24,   1.37,   176.7,    1.77,   178.71,   1.11	},
      	 { 174.58,   0.96,   175.51,   1.31,   178.2,    1.03	},
      	 { 174.91,   1.17,   175.65,   1.28,   177.85,   1.93	},
      	 { 173.55,   1.58,   174.35,   1.29,   176.27,   1.22	},
      	 { 173.47,   1.4,    174.68,   1.54,   176.36,   1.23	},
      	 { 174.68,   1.4,    175.78,   1.66,   177.49,   1.59	},
      	 { 175.1,    1.8,    175.12,   1.15,   177.42,   1.76	},
     	 { 174.7,    1.71,   175.34,   1.55,   176.83,   1.55	}
         },
  
         { 
      	 { 50.68,   1.15,   52.99,   1.79,   54.82,   0.97	},
     	 { 57.86,   1.98,   58.97,   2.64,   60.06,   2.21     },
   	 { 53.41,   1.14,   54.11,   1.63,   56.87,   1.08	},
   	 { 55.46,   1.45,   56.52,   1.89,   59.22,   0.72	},
    	 { 56.18,   1.32,   56.7,    2.06,   60.54,   1.51	},
   	 { 45.05,   1.17,   45.32,   1.2,    47.07,   0.9	},
     	 { 54.76,   1.88,   55.66,   2.04,   59.4,    1.41	},
   	 { 59.85,   1.52,   60.51,   2.11,   64.44,   1.75	},
   	 { 54.99,   0.99,   56.32,   1.87,   59.12,   1.07	},
         { 54.06,   1.17,   55.05,   1.86,   57.42,   1.05	},//Ca
   	 { 54.03,   1.42,   54.51,   1.51,   58.29,   1.54	},
  	 { 52.37,   1.05,   52.91,   1.42,   55.55,   0.98	},
    	 { 62.46,   0.82,   63.54,   1.24,   65.3,    0.99	},
  	 { 54.34,   1.42,   55.74,   1.9,    58.4,    1.03	},
  	 { 54.7,    1.56,   55.86,   2.09,   59.01,   1.19	},
   	 { 57.12,   1.11,   58.41,   1.9,    60.84,   1.17	},
   	 { 60.95,   1.57,   61.54,   2.07,   65.91,   1.53	},
   	 { 60.76,   1.58,   61.84,   2.27,   65.78,   1.46	},
  	 { 56.27,   1.57,   58.03,   1.52,   59.79,   1.79	},
   	 { 56.5,    1.4,    57.63,   2.16,   61.02,   1.91	}
   },

   { 
      { 21.78,   1.8,    18.92,   1.3,    18.18,   1.13 },
      { 29.21,   1.91,   29.67,   1.97,   26.65,   0.99 },
      { 42.87,   1.8,    40.66,   1.37,   40.38,   1.11},
      { 32.64,   1.94,   29.76,   1.63,   29.05,   0.83},
      { 41.72,   1.65,   39.3,    2.09,   38.97,   1.5},
      { 1000.0,  1.0,    1000.0,  1.0,    1000.0,  1.0},//Cb
      { 32.25,   2.54,   29.43,   2.1,    29.59,   1.74},
      { 40.07,   1.82,   38.42,   2.03,   37.3,    1.15},
      { 35.04,   1.7,    32.61,   1.24,   32.11,   0.95},
      { 43.83,   2.03,   41.66,   1.8,    41.23,   1.1},
      { 34.23,   2.42,   32.55,   2.84,   31.42,   1.73},
      { 40.27,   1.93,   38.0,    1.43,   38.22,   1.16},
      { 32.46,   0.95,   31.72,   1.03,   30.93,   0.84},
      { 31.8,    1.83,   28.68,   1.87,   28.16,   0.87},
      { 32.61,   1.79,   30.38,   1.92,   29.87,   0.9},
      { 65.35,   1.45,   63.71,   1.21,   62.77,   0.75},
      { 70.72,   1.39,   69.74,   1.29,   68.26,   1.44},
      { 33.76,   1.81,   32.61,   1.92,   31.23,   0.74},
      { 31.93,   1.75,   29.28,   1.21,   28.8,    1.22},
      { 40.74,   1.89,   38.49,   1.88,   38.2,    1.01}

   },

    { 
      { 125.4,    5.1,    123.58,   3.79,   121.74,   2.63	},
      { 123.0,    5.17,   117.63,   3.79,   117.09,   2.87	},
      { 124.05,   4.97,   120.76,   4.22,   119.96,   1.98	},
      { 123.71,   4.37,   120.67,   3.83,   119.86,   2.67	},
      { 122.04,   4.33,   119.38,   4.94,   118.7,    4.06	},
      { 110.18,   4.44,   109.92,   4.02,   107.47,   2.66	},
      { 122.34,   5.16,   118.61,   5.44,   118.76,   3.41	},
      { 123.74,   4.73,   120.32,   5.56,   120.1,    2.98	},
      { 123.0,    4.85,   121.43,   4.38,   120.08,   3.1	},
      { 125.79,   4.05,   121.92,   4.41,   120.12,   2.38	},//N
      { 121.68,   4.34,   120.35,   3.6,    118.86,   2.29	},
      { 123.13,   3.89,   118.85,   4.65,   117.48,   2.33	},
      { 135,      5,      135.0,    5.0,    135,      5.0	},
      { 123.31,   4.93,   119.55,   3.85,   118.07,   2.29	},
      { 122.54,   5.03,   120.88,   4.57,   119.08,   3.19	},
      { 117.47,   4.31,   115.91,   4.32,   115.02,   2.29	},
      { 118.03,   4.88,   114.11,   5.82,   115.41,   4.19	},
      { 123.35,   5.16,   119.25,   5.96,   119.43,   3.2	},
      { 123.78,   5.52,   121.4,    3.98,   120.15,   3.23	},
      { 122.47,   4.68,   120.04,   4.56,   120.27,   3.26	}
    },

   {
      { 8.57,   0.81,   8.13,   0.65,   7.98,   0.57	},
      { 8.87,   0.83,   7.88,   0.81,   8.14,   0.58	},
      { 8.5,    0.64,   8.41,   0.7,    8.08,   0.52	},
      { 8.68,   0.62,   8.35,   0.61,   8.29,   0.62	},
      { 8.81,   0.73,   7.92,   0.87,   8.16,   0.64	},
      { 8.18,   0.97,   8.34,   0.83,   8.33,   0.8	},
      { 8.89,   0.73,   8.09,   0.88,   8.04,   0.72	},
      { 8.71,   0.67,   7.96,   0.63,   7.98,   0.57	},
      { 8.55,   0.61,   8.12,   0.66,   7.98,   0.62	},
      { 8.64,   0.68,   8.11,   0.77,   8.0,    0.59	},
      { 8.45,   0.68,   8.34,   0.56,   8.08,   0.46	},
      { 8.7,    0.54,   8.41,   0.83,   8.15,   0.58	},
      { 1000.0, 1.0,    1000,   1.0,    1000,   1.0	},//H
      { 8.49,   0.84,   8.21,   0.7,    8.08,   0.59	},
      { 8.55,   0.7,    8.15,   0.83,   8.03,   0.61	},
      { 8.57,   0.65,   8.26,   0.77,   8.17,   0.51	},
      { 8.48,   0.57,   8.22,   0.79,   8.1,    0.54	},
      { 8.74,   0.6,    7.95,   0.76,   7.92,   0.61	},
      { 8.8,    0.75,   7.59,   1.01,   8.14,   0.79	},//101
      { 8.68,   0.76,   7.8,    0.86,   8.08,   0.64	}
   },

   { 
      { 4.89,   0.44,   4.21,   0.36,   4.02,   0.32	},
      { 5.17,   0.48,   4.68,   0.59,   4.25,   0.37	},
      { 5.04,   0.33,   4.64,   0.29,   4.44,   0.27	},
      { 4.81,   0.45,   4.26,   0.29,   3.99,   0.23	},
      { 5.17,   0.51,   4.64,   0.45,   4.1,    0.38	},
      { 4.09,   0.46,   3.95,   0.4,    3.84,   0.43    },
      { 5.09,   0.52,   4.5,    0.52,   4.04,   0.64	},
      { 4.76,   0.42,   4.14,   0.39,   3.7,    0.32	},//Ha
      { 5.02,   0.46,   4.33,   0.32,   3.98,   0.29	},
      { 4.81,   0.44,   4.33,   0.39,   3.96,   0.35	},
      { 4.96,   0.49,   4.64,   0.36,   4.03,   0.32	},
      { 5.28,   0.43,   4.57,   0.4,    4.46,   0.19	},
      { 4.81,   0.47,   4.41,   0.31,   4.11,   0.5	},
      { 4.93,   0.41,   4.3,    0.41,   4.05,   0.24	},
      { 4.88,   0.5,    4.28,   0.42,   4.0,    0.37	},
      { 5.09,   0.49,   4.49,   0.38,   4.2,    0.22	},
      { 4.84,   0.45,   4.34,   0.39,   4.04,   0.3	},
      { 4.67,   0.4,    4.14,   0.42,   3.55,   0.38	},
      { 5.28,   0.41,   4.53,   0.26,   4.44,   0.38	},
      { 5.02,   0.48,   4.58,   0.51,   4.1,    0.4	}
    },
}
;

      double [][] Probab = {
        { 0.291,  0.324,  0.385 },
        { 0.648,  0.185,  0.167 },
        { 0.204,  0.609,  0.187 },
        { 0.321,  0.333,  0.345 },
        { 0.426,  0.278,  0.296 },
        { 0.117,  0.807,  0.076 },
        { 0.333,  0.523,  0.144 },
        { 0.58,   0.237,  0.183 },
        { 0.34,   0.419,  0.241 },
        { 0.41,   0.258,  0.332 },
        { 0.383,  0.277,  0.34  },
        { 0.255,  0.586,  0.159 },
        { 0.198,  0.71,   0.093 },
        { 0.291,  0.379,  0.33  },
        { 0.31,   0.357,  0.333 },
        { 0.39,   0.466,  0.143 },
        { 0.496,  0.366,  0.137 },
        { 0.615,  0.166,  0.219 },
        { 0.676,  0.155,  0.169 },
        { 0.551,  0.281,  0.168 }
      };

//------------------------------------------------------------------------------------------------------------------



//--Protein Identifiers------------------------------------------------------------------------------------------

	String AminoAcids = "ACDEFGHIKLMNPQRSTVWY";
	String[] Atoms = new String[] {"C","CA","CB","N","H","HA"};
	String secondaryStructures = "BCH";

	public int aminoToInt(String a) {
		int returned=-1;
		try {
		returned =AminoAcids.indexOf(a);
		} catch(Exception e){;}
		return returned;
	}

	public int atomToInt(String a) {
		int returnValue = -1;
		for(int b=0;b<Atoms.length;b++) {
			if(Atoms[b].equals(a))
				returnValue = b;
		}
		return returnValue;
	}

//-----------------------------------------------------------------------------------------------------------------
//--PDF functions--------------------------------------------------------------------------------------
//201
	private Double pdfequation(String csi, Double mean, Double dis) {
		try {
		Double x=Double.parseDouble(csi);
		Double pdf1 = (1/(dis*(Math.sqrt(2*3.14)))*(Math.exp(-((x-mean)*(x-mean))/(2*dis*dis))));
		return pdf1;
		} catch (Exception e) {return 1.0;}
	}



	public Double pdf (String amino, String atom, int structure, String csi) {

               // System.out.println("G pdf  "+ amino  +" "+ atom+ "  "+ Integer.toString(structure)+"  "+csi);

		return pdfequation( csi,CSIDIS[atomToInt(atom)][aminoToInt(amino)][structure*2],
                                        CSIDIS[atomToInt(atom)][aminoToInt(amino)][structure*2+1]);
	}

//----------------------------------------------------------------------------------------------------------------
//--Retrieving Functions  single atom ---------------------

	public Double[][] retrieveSingleAtom  (String csi, String atom) {
		Double[][] probabilities = new Double[AminoAcids.length()][3];
		for (int a=0;a<AminoAcids.length();a++) {
			for (int b=0;b<3;b++) {
				probabilities[a][b] = pdf(AminoAcids.substring(a,a+1),atom,b,csi);
			}	
		}
		return probabilities;
		
	}

//.............. only take best sec str  10_20_2009

	protected Double[] pdf_SingleAtom_best_sec  (String csi, String atom) {

		Double[] atom_prob = new Double[AminoAcids.length()];// 20 amino acid
		for (int a=0;a<AminoAcids.length();a++) {
			   double p_b = pdf(AminoAcids.substring(a,a+1),atom,0,csi);
                           double p_c = pdf(AminoAcids.substring(a,a+1),atom,1,csi);
                           double p_h = pdf(AminoAcids.substring(a,a+1),atom,2,csi);
			   atom_prob [a]=p_c; 
                           if (p_b>p_c)
                                    atom_prob [a]=p_b; 
                           if (p_h>p_c)
                                    atom_prob [a]=p_h;	
		}
		return atom_prob;	
	};

	protected Double[] pdf_Single_Res (String [] CO_CA_CB_N_H_HA) {

		Double[] Res_prob = new Double[AminoAcids.length()];// 20 amino acid
		for (int a=0;a<AminoAcids.length();a++) {
                    Res_prob[a]=1.0;
                    for (int b=0; b<Atoms.length; b++)  
                        Res_prob [a] = Res_prob[a]*pdf_SingleAtom_best_sec (CO_CA_CB_N_H_HA [b], Atoms[b]) [a];	    
		}
		return Res_prob;	
	};


//....................................................................................

//////////////////
 
	public Double[][] retrieveMultiple(String[] data) {
		Double[][][] probability = new Double[6][20][3];
		Double[][] probabilities = new Double [20][3];

		for(int c=0;c<probability.length;c++)
			probability[c] = retrieveSingleAtom(data[c],Atoms[c]);

		for(int z=0;z<probabilities.length;z++) {
			for (int x=0;x<probabilities[0].length;x++) {
				probabilities[z][x]=Probab[z][x]*probability[0][z][x]*probability[1][z][x]*probability[2][z][x]*
						probability[3][z][x]*probability[4][z][x]*probability[5][z][x];
				 
			}
		}
		return probabilities;
	}
 
////////////////


	public Double[][][] aminoStructureProbabilities(String[][] data) {
		Double[][][] probabilities = new Double [data.length][20][3];
		for(int a=0;a<data.length;a++) 
			probabilities[a] = retrieveMultiple(data[a]);
		return probabilities;
	}



		
	public Double[][][] aminoStructureProbabilities(String[][] data,boolean[] usage) {
		Double[][][] probabilities = new Double [data.length][20][3];
		for(int a=0;a<data.length;a++) {
			for(int b=0;b<6;b++) {
				if(!usage[b])
					data[a][b]="--";
				//System.out.print(data[a][b]+"    ");
			probabilities[a] = retrieveMultiple(data[a]);
			}
			//System.out.println("");
		}
		return probabilities;
	}

	                     //shorter num, seq, pre-seq, "C","CA","CB","N","H","HA"   10_18_2009
                                //      0    1     2       3    4    5   6    7   8
//.......................10_20_2009

        public Double [][] get_seq_amino_Prob (String[][] seq_csi) { // input strReader shorter
          //System.out.println("Gaussin  "+ Integer.toString(seq_csi.length) + "  " +Integer.toString( AminoAcids.length()) )  ;
  
          Double[][] seq_amin = new Double [seq_csi.length][AminoAcids.length()];
            for (int seq=0; seq<seq_amin.length; seq++) {
                     String [] CO_CA_CB_N_H_HA = {seq_csi[seq][3], seq_csi[seq][4],seq_csi[seq][5],seq_csi[seq][6], seq_csi[seq][7],seq_csi[seq][8]};
                     // System.out.println("G get  "+ seq_csi[seq][3]+"  "+ seq_csi[seq][4]+"  "+ seq_csi[seq][5]+"  "+ seq_csi[seq][6] +"  "+ seq_csi[seq][7]+"  "+ seq_csi[seq][8]);
	 
		      for (int aa=0; aa<seq_amin[0].length; aa++)  
                        seq_amin[seq][aa] = pdf_Single_Res(CO_CA_CB_N_H_HA)[aa];  
            };
		return seq_amin;
         };


// new function 11-18-2009, Suspicous range added, input times of standard deviation...
// out put  1. {"C","CA","CB","N","H","HA"}; 2. "ACDEFGHIKLMNPQRSTVWY"; 3. min and max accepted value      

	public Double[][][] get_suspiou_rang (double time_dev) {

		Double[][][] range  = new Double [Atoms.length][AminoAcids.length()][2];
                for (int a=0; a<range.length; a++) {

                    for (int n=0; n<range[0].length; n++) {
                         if (a<2) {
                           range[a][n][0] = CSIDIS[a][n][0] - time_dev*CSIDIS[a][n][1];
                           range[a][n][1] = CSIDIS[a][n][4] + time_dev*CSIDIS[a][n][5];
                           }
                         else {
                           range[a][n][1] = CSIDIS[a][n][0] + time_dev*CSIDIS[a][n][1];
                           range[a][n][0] = CSIDIS[a][n][4] - time_dev*CSIDIS[a][n][5];
                         };
                      // calibration for Csy CB-- reduce and oxid status
                      if (a==2 && n==1)  
                            
                             range[a][n][1]+= 14.0;
       
                    
                      
                    };
                };
/*
    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    nf.setMinimumFractionDigits(2);


           for (int q=0; q<range.length;q++) {
               for (int w=0; w<range[0].length;w++) {
                 String tmp="";
                 for (int e=0; e<range[0][0].length; e++)
                      tmp +=nf.format(range[q][w][e])+" ";
                // System.out.println(tmp);
               };
           };

*/

          return range;        
		 
	};






}