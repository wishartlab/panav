package standalone;

// 10_21_2000, 1.get overall score  2.suspisous assig and suggested with relative probility.
// inputs for constructor are all from strReader
//  1. sequence and index transfer to Gaussing 20 amino acids, 
//  2. sixty() (ref calibrated or no caliberted) 
//  3. allow, 10% between first and second,
//  4. w/o slected fragments (based on CA CB shifts)

import java.text.NumberFormat;
 

public class Scanner_3 {

    NumberFormat nf = NumberFormat.getInstance();
    
     int [] s_position; // these four array to save position, probability, for original and sugested, 
                        // confirm assigned is marked as -2 in s_position. 
     
     Double [] o_probab;
     Double [] s_probab;

     String [] Score = new String [3];
           // 1 selected frag #, 2 confirmed frag #, 3 percentage confirmed #/selected #

  
     int modlength;
       String seq;

//--Constructor 1 sequence, 2  selected fragments with enough CA and CB shifts aa_index, sixty boolean from strReader_2  -----------------------------------------------------

    public Scanner_3 (String sequence, int[] aa_index, Double[][] sixty, int m_len, Double allow, boolean[] CaCb) {
     
     //  System.out.println(Integer.toString(sequence.length())+ "  "+Integer.toString(aa_index.length)
     //                   + "  "+Integer.toString(sixty.length) + "  "+Integer.toString(CaCb.length) );

       modlength=m_len;
       seq = sequence;

       int num_s = aa_index.length-modlength+1;  // numbers of possible scan
    
       s_position= new int[num_s];
       for (int o=0; o<s_position.length; o++)
           s_position[o]=-2;

       o_probab= new Double [num_s];
       s_probab=new Double[num_s];
	
       nf.setMaximumFractionDigits(2);
       nf.setMinimumFractionDigits(2);
   
       int Selected=0;
       int Confirmed=0;
  
   
 // to fill o_probab 
       
       
       for (int o=0; o<num_s; o++) {
        
        if(CaCb[o]){

            double tmp =1.0;
            for (int q=0; q<modlength; q++) {
             try { tmp*= sixty[o+q][aa_index[o+q]];
                 } catch (Exception e0) {

                   // System.out.println("to fill o_probab  " + Integer.toString(o+q)+"  "+Integer.toString(aa_index[o+q])+" "+Integer.toString(aa_index.length));
                   // if abnormal amino acid, e.g. X  4146 5179 4700 .... prob is ignored.
                  }
            };
            o_probab[o]=tmp;
        };
   
      };

// to scan 
 
       for (int i=0; i<num_s; i++) {

           Double[] store_prob= new Double [num_s];// used to for scan  entire seq
           int[] store_pos= new int [num_s];              

           if(CaCb[i]) {

            Selected +=1.0; 

            for (int s=0; s<num_s; s++) {
                 double frg_prob = 1.0;

                 for (int f=0; f<modlength; f++) 
                     try {  
                     frg_prob*= sixty[i+f][aa_index[s+f]]; 
                         } catch (Exception e1) {
                               //System.out.println("to scan   "+ Integer.toString(s+f)+"  "+Integer.toString(aa_index.length));
                                }
                  store_prob[s]=frg_prob; 
                  store_pos[s]=s;   
                   
            };

           
            
            Double  h_prob = o_probab[i]; //highest probility in scan initiat value set to itself
            int h_pos =i  ;
        
 
            for (int g=0;g<store_prob.length; g++) { // scan for highest prob
                Double tmp = store_prob[g]; 
                if (tmp > h_prob ) {
                  h_prob = tmp;
                  h_pos=g;
                };
            }; 

          
           boolean confirmed_a = true;
 
           if ((h_pos != i) && (h_prob>(1.0+allow)*o_probab[i]))
                confirmed_a = false;
 
           if ((CaCb[h_pos]) && ( h_prob < (1.0+allow)*o_probab[h_pos]))   {
                confirmed_a = true;
               //System.out.println("yes");
            };
 

            if(confirmed_a)
              Confirmed +=1.0;
            else { s_position[i]=h_pos; 
                   s_probab[i]=h_prob;           
            };





 
         };//eof if CaCb
       }; //eof i

      

       Score[0] = Integer.toString(Selected);
       Score[1] = Integer.toString(Confirmed);
       Score[2] = nf.format(Confirmed*100.0/Selected)+"%";

    
};    

  public String [] get_Score () {
          return Score;
  };

  
  public int [] get_s_pos () {

     return s_position;
  };


  public int [] get_o_pos () {

     int[] tmp = new int [s_position.length];
     for (int p=0; p<tmp.length; p++)
        tmp[p]=p;

     return tmp;  
  };



     
  public Double [] get_o_prob () {
     return o_probab;
 };

  public Double [] get_s_prob() {

     return s_probab;
  };

  
  public String [] get_o_prob_nor() {
       nf.setMaximumFractionDigits(2);
       nf.setMinimumFractionDigits(2);



        String[] nor = new String [o_probab.length];

        for (int n=0; n<nor.length; n++) {
            
            try {nor[n]=nf.format(o_probab[n]*100.0/s_probab[n])+"%";
                 } catch (Exception e) {;}
           // System.out.println(nor[n]);
         }   
        return nor;
  };


 public String get_message () {
   String mess="";
    
   
    for (int i=0; i<s_position.length; i++) {
         if (s_position[i]>-1) {
          try {
          String o_frag  =seq.substring(i,i+modlength) + "("+Integer.toString(i+1) +"-" +Integer.toString(i+modlength) +")"; 
          String s_frag  =seq.substring(s_position[i],s_position[i]+modlength) + "("+Integer.toString(s_position[i]+1) +"-" +Integer.toString(s_position[i]+modlength) +")"; 

          String o_prob=get_o_prob_nor()[i];
          String s_prob="100%";
          
          
          mess+= o_frag +"  " + o_prob +  "  " + s_frag +"  "+ s_prob +"\n";
            } catch (Exception e1) {;}
         };      
     }; 
 
    

  // System.out.println(mess);
   return mess;

 };//eof get_message
      
}