package standalone;

import javax.swing.table.AbstractTableModel;

public class chemicalShift extends AbstractTableModel {

	Object[][] data;
	int length=0;

	private String[] headings = new String[] {"Seq","C","CA","CB","N", "H","HA", "Beta sheet probability","Coil probability","Helix probability", "SecStr"};//10_19_2009
                                                 // 0    1    2    3   4    5    6     7         8        9          10
	public int getRowCount() {
		return data.length;
	}

	public int getColumnCount() {
		return headings.length;
	}	

	public Object getValueAt(int row, int column) {
		return data[row][column];
	}

	public String getColumnName(int column) {
		return headings[column];
	}

	public chemicalShift (Object[][] data) {
		this.data = new Object[data.length][12];

		this.data = data;
		
	}


}