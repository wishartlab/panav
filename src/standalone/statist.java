package standalone;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.Vector;

public class statist {

  public   int decPlace = 2;   

  
  public double ave, std, min, max;
  public double rsmd_cut;

  public statist() {
         rsmd_cut=0.0;
  }

  public statist (double cut) {
    rsmd_cut=cut;
  }


 public String[] coef_ave_std (Vector xy) {

    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(3);
    nf.setMinimumFractionDigits(3);



    String[] dxy = new String[xy.size()];
    for(int i=0; i<dxy.length; i++) {
        StringTokenizer st = new StringTokenizer(xy.elementAt(i).toString()) ;
        String[] a = new String[st.countTokens()];
        int l=0;
        while (st.hasMoreTokens()) {a[l]=st.nextToken(); l++;};

        dxy[i]=nf.format(Double.parseDouble(a[0])-Double.parseDouble(a[1]));
    };

    String[] res = analyse(dxy);
    double cutMin=Double.parseDouble(res[0])-rsmd_cut*Double.parseDouble(res[1]);
    double cutMax=Double.parseDouble(res[0])+rsmd_cut*Double.parseDouble(res[1]);

    for (int j=0; j<xy.size(); j++) {
         if(Double.parseDouble(dxy[j]) <cutMin || Double.parseDouble(dxy[j])>cutMax )  
         xy.remove(j);
    };

    
    String[] d_xy=new String[xy.size()]; double x_ave=0, y_ave=0;
                                         double x_sum=0, y_sum=0;
    for (int k=0; k<xy.size(); k++) {

        StringTokenizer st = new StringTokenizer(xy.elementAt(k).toString()) ;
        String[] a = new String[st.countTokens()];
        int l=0;
        while (st.hasMoreTokens()) {a[l]=st.nextToken(); l++;};

        d_xy[k]=nf.format(Double.parseDouble(a[0])-Double.parseDouble(a[1]));
        x_sum+=Double.parseDouble(a[0]); y_sum+=Double.parseDouble(a[1]);
      
    };
    
    String[] ave_std = analyse(d_xy);
    x_ave=x_sum/xy.size(); y_ave=y_sum/xy.size();

    double sum_xxyy=0;double sum_xx2=0; double sum_yy2=0;

    for (int f=0; f<xy.size(); f++) {
      
        StringTokenizer st = new StringTokenizer(xy.elementAt(f).toString()) ;
        String[] a = new String[st.countTokens()];
        int l=0;
        while (st.hasMoreTokens()) {a[l]=st.nextToken(); l++;};

        sum_xxyy+=(Double.parseDouble(a[0])-x_ave)*(Double.parseDouble(a[1])-y_ave);

        sum_xx2+=(Double.parseDouble(a[0])-x_ave)*(Double.parseDouble(a[0])-x_ave); 
        sum_yy2+=(Double.parseDouble(a[1])-y_ave)*(Double.parseDouble(a[1])-y_ave); 
    };
 
    String coef = nf.format(Math.sqrt(sum_xxyy*sum_xxyy/(sum_xx2*sum_yy2)));

    String[] coef_ave_std = new String[3];
   
    coef_ave_std[0]=coef; coef_ave_std[1]=ave_std[0]; coef_ave_std[2]=ave_std[1];

    return coef_ave_std;

 };






  public String[] StatAna (Vector input) {

     String[] in = new String[input.size()];

     for (int i=0; i<in.length; i++)
         in[i]=input.get(i).toString();


     String[] res = analyse(in);

      double cutMin=Double.parseDouble(res[0])-rsmd_cut*Double.parseDouble(res[1]);
      double cutMax=Double.parseDouble(res[0])+rsmd_cut*Double.parseDouble(res[1]);

     double[] check = removeEmpty(in);
     String[] reinput=new String[check.length];
     boolean flg = true;
       for (int t=0; t<check.length; t++) {
         if ((check[t]<cutMin)||(check[t]>cutMax)) {
            flg=false;
            reinput[t]="";}
         else
            reinput[t]=Double.toString(check[t]);
     };// for

     if(flg) 
        return res;
     else 
        return analyse(reinput);
     
     };// method
      
     

  public String[] analyse (String[] str) {
    double[] data = removeEmpty(str);
    double sum = 0.0;
    double var = 0.0;
    
    double ave=0.0;
    double std=0.0;
    int num=0;
    double min=0.0;
    double max=0.0;
   
    num=data.length;
    Arrays.sort(data);
     

    if (num>0) {

       min = data[0];
       max = data[num-1];


       for (int j=0;j<num;j++)
         sum +=data[j];
    
       ave = sum/num;
   
       for (int m=0;m<num;m++)
         var += Math.abs(data[m]-ave)*Math.abs(data[m]-ave);
    
       if(num>1)
         std = Math.sqrt(var/(num-1));
       else
         std = 0.0;
     };// if num>0


    String[] result = new String[5] ;   

    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(decPlace);
    nf.setMinimumFractionDigits(decPlace);
   
    result[0] = nf.format(ave);
    result[1] = nf.format(std);
    result[2] = Integer.toString(num);
    result[3] = nf.format(min);
    result[4] = nf.format(max);

    return result;
 
    } // end of method
 

public double[] removeEmpty(String[] in) {
 
     
Vector tmp = new Vector();

for (int m=0;m<in.length;m++ ) {
  
    try {  if (!(in[m].trim().equals(""))) {  
           Double.parseDouble(in[m]);
           tmp.add(in[m]);};
         } catch (Exception e) {};
    
};

double [] dtmp= new double[tmp.size()];

for (int p=0;p<dtmp.length;p++) 
     dtmp[p]= Double.parseDouble(tmp.get(p).toString());

  return dtmp;
 };// method

}///~    
    
		

 
  
                     