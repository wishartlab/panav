package standalone;
// 10_22_2009, data was modified, before trimed those with no chemical shifts, new one --- just trim end on seq from 2000 max

import java.io.*;
import java.lang.Object.*;
import java.util.StringTokenizer;
import java.util.*;
import java.util.regex.*;
import java.text.*;


public class CSFileReader {
  
  public static boolean TESTCSPARSER = false;
      
  String AA="ACDEFGHIKLMNPQRSTVWY";
  String[] Atoms = new String[] {"C","CA","CB","N","H","HA"};

  String[][] Raw_data; // num, res, pre-res, "C","CA","CB","N","H","HA"  raw csi data 10_18_2009
                       //  0    1      2      3    4    5   6   7    8 

  String[][] No_deviant_data;// 11_6_2009 
  Gaussian ga = new Gaussian();



  String D_data= "";
  Double d_time = 6.0;

  Double[][][] d_range = ga.get_suspiou_rang(d_time);


  String S_data= "";  //after remove Deviant data removed, suspious data remains
                      //in secondary structure ID and Assign Validation
  Double s_time=3.5;
  Double[][][] s_range = ga.get_suspiou_rang(s_time);

  int Total_CSI=0;
  int Total_S=0;
  int Total_D=0;


  //----------------------------------

  public boolean need_ref_cal;
  public String ref_shift;

  Boolean missed =false;

  String Sequence="";// input sequence

  CSI_Sec csi_Se;

  String[][] secstr_no_cal;
  String[][] secstr_with_cal;

  NumberFormat nf = NumberFormat.getInstance();
  DecimalFormat df = new DecimalFormat("0.0000");
  CSI_AVE_STD csid = new CSI_AVE_STD();



  String sub_year="";
  String sub_pdb1="NA";
  // A original sequence and assignments;
  String SeQuence="";
  String[][]  seqCSI;

  // B abnormal chemical shifts  typo mistaken csi  Removed  those not in the csi_nu_range 
  //   this or C can be used for secondary structure id, and assig validation -- give the list in both appl.

  String[][] No_Abnormal_seqCSI;
  String Abnormal_csi_List="";
  String No_Ab_CSI_num="";
  int Abnormal_csi_num=0;

  // C related to B, after remove abnormal chemical shifts, chemical shifts ref-calibrated seq csi
  //   This will be used for ss id and assign vali


  String[][] No_Abnormal_RefCal_seqCSI;
  String Ref_Cal="";  

  // D Before and After ref_Cal, residue specific our range chemical shifs after (NOT inluding abnormal csi)... this is to give the varning sign of chemical shift assignment.
  String Before_refCal_Res_out_List="";
  int    Before_refCal_Res_out_num=0;

  String After_refCal_Res_out_List="";
  int    After_refCal_Res_out_num=0;

  ////////////////////////  

  //	boolean startCollecting;

  Gaussian base = new Gaussian();

  private static final String BMRB2_SEQ_SECTION_START = "_Mol_residue_sequence";
  private static final String BMRB3_SEQ_SECTION_START = "_Entity.Polymer_seq_one_letter_code";
  //private static final String BMRB2_AFTER_SEQ_SECTION = "loop_";
  //private static final String BMRB3_AFTER_SEQ_SECTION = "_Entity.Target_identifier";
  private static final String BMRB2_BEFORE_CS_TABLE = "_Chem_shift_ambiguity_code";
  private static final String BMRB3_BEFORE_CS_TABLE = "_Atom_chem_shift.Assigned_chem_shift_list_ID";
  
  private static final int BMRB_SEQ_NUM_LOOKUP = 0; // index in bmrbVersion2Labels or bmrbVersion3Labels
  private static final int BMRB_RES_LABEL_LOOKUP = 1; // index in bmrbVersion2Labels or bmrbVersion3Labels
  private static final int BMRB_ATOM_NAME_LOOKUP = 2; // index in bmrbVersion2Labels or bmrbVersion3Labels
  private static final int BMRB_CS_VAL_LOOKUP = 3; // index in bmrbVersion2Labels or bmrbVersion3Labels
  private static final int NEF_MOL_SYS_CHAIN_CODE_LOOKUP = 0; // index in nefLabels
  private static final int NEF_MOL_SYS_SEQ_CODE_LOOKUP = 1; // index in nefLabels
  private static final int NEF_MOL_SYS_RES_LABEL_LOOKUP = 2; // index in nefLabels
  private static final int NEF_CS_TBL_CHAIN_CODE_LOOKUP = 3; // index in nefLabels
  private static final int NEF_CS_TBL_SEQ_CODE_LOOKUP = 4; // index in nefLabels
  private static final int NEF_CS_TBL_RES_LABEL_LOOKUP = 5; // index in nefLabels
  private static final int NEF_CS_TBL_ATOM_NAME_LOOKUP = 6; // index in nefLabels
  private static final int NEF_CS_TBL_CS_VAL_LOOKUP = 7; // index in nefLabels

  private static final String[] bmrbVersion2Labels = {
                                  "_Residue_seq_code",  // Sequential integer starting from 1.
                                  "_Residue_label",     // 3-letter amino acid code
                                  "_Atom_name",         // Atom name (e.g. "HA", "HB2")
                                  "_Chem_shift_value"}; // Chemical shift value.
  private static final String[] bmrbVersion3Labels = {
                                  "_Atom_chem_shift.Seq_ID", // Sequential integer starting from 1.
                                  "_Atom_chem_shift.Comp_ID",
                                  "_Atom_chem_shift.Atom_ID",
                                  "_Atom_chem_shift.Val"};
  private static final String bmrb3CompIdLabel = "_Entity_comp_index.Comp_ID"; // 3-letter amino acid code
  private static final String[] nefLabels = {
                                  "_nef_sequence.chain_code", // Chain code in molecular system section.
                                  "_nef_sequence.sequence_code", // Sequence code in molecular system section, which is a string, not an integer.
                                  "_nef_sequence.residue_type", // 3-letter sequence abbreviation in molecular system section
                                  "_nef_chemical_shift.chain_code", // Chain code in chemical shifts table.
                                  "_nef_chemical_shift.sequence_code", // Sequence code in chemical shifts table, which is a string, not an integer.
                                  "_nef_chemical_shift.residue_type",
                                  "_nef_chemical_shift.atom_name",
                                  "_nef_chemical_shift.value"};

  
  private Vector <String> molecularSystemLines = new Vector <String> ();
    // For NEF files. Collection of all lines in the main molecular system table.



  String[] Three_AA = {"ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU",
                         "MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"};

  String[] Single_AA = {"A",  "C",  "D",  "E",   "F",  "G",  "H",  "I",  "K",  "L",
                         "M",  "N",  "P",  "Q",   "R",  "S",  "T",  "V",  "W",  "Y"};



  public boolean isAmino(String amino) {
    boolean is = false;
    for(int a=0;a<Three_AA.length;a++) {
      if(Three_AA[a].equals(amino))
        is=true;
    }
    return is;
  }


  // following just transfer three letter to one letter, code need to improve 10_18_2009

  public String three_to_one (String t) {


    String returned = "";
    int ind = -1;

    for(int a=0;a<Three_AA.length;a++) {
      if(Three_AA[a].equals(t)) {
        ind = a;
        break;
      };
    };

    if(ind>-1)
      returned = Single_AA[ind];
    else
      returned ="X"; // 7/4/10 for abnormal amino acid

    return returned;
  }



  // constructor
  public CSFileReader (File strfile, String file_type) {
    
    nf.setMaximumFractionDigits(4);


    if (file_type.equals("str") || file_type.equals("nef")) {

      int linenumber = 0;
      int startnumber = 0;

      int[] location;
        // Indices of the columns in the chemical shifts table (starting from 0 for first column),
        // or, in the case of NEF files, indices of the columns of either the molecular system
        // table or the chemical shifts table (starting from 0 for the first column).
        // The elements are the indices of the columns in the input file's chemical shifts table
        // and/or molecular system table that correspond to the labels in bmrbVersion2Labels,
        // bmrbVersion3Labels, or nefLabels.
      if (file_type.equals("str")) {
        location = new int[4];
      } else { // file_type.equals("nef")
        location = new int[nefLabels.length];
      }
      for (int i = 0; i < location.length; i++) {
        location[i] = -1;
      }
      

      missed=false;
      Vector <String> csTableLines = new Vector <String> ();

      boolean inLoop = false;
      boolean parsingColumnLabels = false;
      int numColumns = 0; // Number of columns in present loop.
      int bmrb3CompIdIdx = -1; // column index
      boolean doneSequenceSection=false;
      boolean inMolecularSystemSectionNEF = false;
      boolean doneMolecularSystemSectionNEF = false;
      String firstChainCode = null;
      boolean inChemShiftTable = false;
      boolean doneChemShiftTable=false;

      try {
        FileReader fr = new FileReader(strfile);
        BufferedReader in = new BufferedReader(fr);
        String line;

        while ((line=in.readLine())!=null) { // Main loop

          linenumber++;
          
          if (Pattern.matches("(?i)^loop_\\b", line.trim())) {
            startnumber = linenumber;
            inLoop = true;
            parsingColumnLabels = true;
            continue;
          }

          if (Pattern.matches("(?i)^stop_\\b", line.trim())) {
            inLoop = false;
            numColumns = 0;
            
            if (!Sequence.equals("")) {
              doneSequenceSection = true;
            }
            
            if (inMolecularSystemSectionNEF) {
              doneMolecularSystemSectionNEF = true;
              inMolecularSystemSectionNEF = false;
              continue;
            }
            
            if(inChemShiftTable) {
              doneChemShiftTable=true;
            }
            inChemShiftTable=false;
            
            continue;
          }

          if (inLoop) {
            
            if (Pattern.matches("^\\s*$", line) || Pattern.matches("^\\s*\\#", line.trim())) {
              // blank line or comment
              continue;
            } else if (line.trim().startsWith("_")) {
              numColumns++;
              
              if (Pattern.matches("(?i)^\\s*" + bmrb3CompIdLabel + "\\b", line.trim())) {
                bmrb3CompIdIdx = linenumber - startnumber - 1;
                continue;
              }
              
              for(int i=0;i<location.length;i++) {
                if (file_type.equals("str")) {
                  if (Pattern.matches("(?i)^\\s*" + bmrbVersion2Labels[i] + "\\b", line.trim()) ||
                      Pattern.matches("(?i)^\\s*" + bmrbVersion3Labels[i].replace(".", "\\.") + "\\b", line.trim())) {
                      // (?i) is to make the matches case-insensitive
                    location[i] = linenumber - startnumber - 1;
                  }
                } else { // file_type.equals("nef")
                  if (Pattern.matches("(?i)^\\s*" + nefLabels[i].replace(".", "\\.") + "\\b", line.trim()) ) {
                      // (?i) is to make the matches case-insensitive
                    location[i] = linenumber - startnumber - 1;
                  }
                }
              }
              
            } else {
              parsingColumnLabels = false; // we're done parsing the list of column labels for the current loop
            }
            
          }
          
          if (inLoop && !parsingColumnLabels) {
            if (!doneSequenceSection) {
              
              if (bmrb3CompIdIdx != -1) {
                // We're in a BMRB v3.1 Entity_comp_index loop. Parse the sequence.
                String[] elems = line.trim().split("\\s+");
                Sequence += three_to_one(elems[bmrb3CompIdIdx]);
                
              } else if (location[0] != -1 && location[1] != -1 && location[2] == -1) {
                // For BMRB v2.1.
                // _Residue_seq_code and _Residue_label were found, but not _Atom_name. This suggests were are in the sequence table.
                String[] elems = line.trim().split("\\s+");
                
                int resLabelIndex = location[1];
                for (int i = 0; i < elems.length; i++) {
                  if (i % numColumns == resLabelIndex) {
                    Sequence += three_to_one(elems[i]);
                  }
                }
                
              }
            }
          }
          
          if ( !doneMolecularSystemSectionNEF && Pattern.matches("(?i)^\\s*(_nef_sequence\\.\\w+)", line.trim()) ) {
            // We are in the molecular system section of a NEF file.
            inMolecularSystemSectionNEF = true;
            continue;
          }
          
          if (inMolecularSystemSectionNEF) {
            if (Pattern.matches("^\\s*\\S+\\s+\\S+.*?", line.trim())) {
              molecularSystemLines.add(line.trim());
            }
          }

          if (Pattern.matches("(?i)^" + BMRB2_BEFORE_CS_TABLE + "\\b", line.trim()) ||
              Pattern.matches("(?i)^" + BMRB3_BEFORE_CS_TABLE.replace(".", "\\.") + "\\b", line.trim()) ||
              Pattern.matches("(?i)^_nef_chemical_shift\\.\\w+\\b", line.trim())
              ) {
            inChemShiftTable=true;
            continue;
          }
          
          if(!doneChemShiftTable && inChemShiftTable) {
            if (!(Pattern.matches("^\\s*$", line.trim()) ||
                Pattern.matches("^\\s*\\#.*", line.trim()) ||
                Pattern.matches("(?i)^_nef_chemical_shift\\.\\w+\\b", line.trim()))
                ) {
              
              if (file_type.equals("nef")) {
                String[] elems = line.trim().split("\\s+");
                String chainCode = elems[location[NEF_CS_TBL_CHAIN_CODE_LOOKUP]];
                String seqCode = elems[location[NEF_CS_TBL_SEQ_CODE_LOOKUP]];
              
                if (Pattern.matches(".*\\@.*", seqCode)) {
                  // Unassigned resonance. Skip it.
                  continue;
                }
              
                if (firstChainCode == null) {
                  firstChainCode = chainCode;
                }
              }
              
              csTableLines.add (line); // Line from main chem shift table, so add it to csTableLines
            }
          }
        
        };

        in.close();
        fr.close();

        if (file_type.equals("nef")) {
          // For NEF files, we only want to parse the first chain in the chemical shifts
          // table (ignoring lines with unassigned resonances). That means we want to
          // parse the full sequence from the molecular system table for the same chain.
          // But we cannot be guaranteed that the first chain in the molecular system
          // table is the first chain in the chemical shifts table. Therefore, above we
          // determined the first chain in the chemical shifts table and stored it in
          // firstChainCode. Now we will parse the molecular system table for the full
          // sequence of that chain.
          
          for (int i = 0; i < molecularSystemLines.size(); i++) {
            String msLine = molecularSystemLines.elementAt(i);
            
            String[] elems = msLine.trim().split("\\s+");
            String chainCode = elems[location[NEF_MOL_SYS_CHAIN_CODE_LOOKUP]];
            String seqCode = elems[location[NEF_MOL_SYS_SEQ_CODE_LOOKUP]];
            
            if (chainCode.equals(firstChainCode)) {
              String res_code_multi_char = elems[location[NEF_MOL_SYS_RES_LABEL_LOOKUP]];
              if (!res_code_multi_char.equalsIgnoreCase("TENSOR")) {
                // A proposal for the NEF specification allows tensor dummy residues to be
                // included in the molecular system table. Ignore them here.
                
                String res_code = three_to_one(res_code_multi_char);
                Sequence += res_code;
              }
            }
          }
          
        }


        Sequence.trim();
        //System.out.println("Sequence Length: " + Sequence.length());


        Raw_data = new String[Sequence.length()][9];


        try{
          Fill_csi_table (csTableLines, location, file_type);
        } catch (Exception ess) {
          ess.printStackTrace();
        }

        Remove_deviant_data (Raw_data);//fill No_deviant_data too, and also cal suspious data.

        csi_Se = new CSI_Sec (No_deviant_data);

        Find_s_data (csi_Se.get_ref_seqCSI());

        secstr_no_cal=csi_Se.get_Sec_No_Cal();
        secstr_with_cal=csi_Se.get_Sec_with_Cal();

      }catch (Exception e) {
        e.printStackTrace();
      }

    };// eof file_type


    if (file_type.equals("shiftX_table")) {

      missed=false;


      boolean foundHeader = false;

      Vector <String> tmp = new Vector <String>();

      try {
        FileReader fr = new FileReader(strfile);
        BufferedReader in = new BufferedReader(fr);
        String line;
        Pattern headerPattern = Pattern.compile("^\\s*(?:>|#?(?:NUM)?)\\s+(?:AA|RES)\\s+(.*)");
        String[] columnLabels = null;
        while ((line=in.readLine())!=null) {
          
          if (Pattern.matches("^\\s*$", line) || (line.trim()).startsWith("--")) {
            // skip line
          } else {
            
            Matcher m = headerPattern.matcher(line);
            if (m.matches()) { // header line
              
              if (!foundHeader) {
                columnLabels = m.group(1).split("\\s+");
                foundHeader = true;
              } else {
                // We found a second header, indicating a new table within the SHIFTX file. Ignore it.
                break;
              }
            } else if (foundHeader) {
              tmp.add(line.trim());
            }
          }
          
        };// eof while
        
        

        Raw_data = new String [tmp.size()][9];

        for (int i=0; i<Raw_data.length; i++) {
          
          String[] elems = tmp.elementAt(i).split("\\s+");
          
          Raw_data[i][0] = elems[0].trim();
          Raw_data[i][1] = elems[1].trim();
          if(i>0) {
            Raw_data[i][2] = Raw_data[i-1][1];
          }
          else {
            Raw_data[i][2] ="";
          }
          
          try {
            for (int colIndex = 0; colIndex < 6; colIndex++) {
              String colLabel = columnLabels[colIndex];
              
              int targetIndex = -1; // index in Raw_data
              if (colLabel.equals("C") || colLabel.equals("CO")) {
                targetIndex = 3;
              } else if (colLabel.equals("CA")) {
                targetIndex = 4;
              } else if (colLabel.equals("CB")) {
                targetIndex = 5;
              } else if (colLabel.equals("N")) {
                targetIndex = 6;
              } else if (colLabel.equals("H") || colLabel.equals("HN")) {
                targetIndex = 7;
              } else if (colLabel.equals("HA")) {
                targetIndex = 8;
              } else {
                System.err.println("Unrecognized column label " + colLabel);
                System.exit(1);
              }
              
              int sourceIndex = colIndex + 2; // index in SHIFTX table
              if (Double.parseDouble(elems[sourceIndex])>1.0) {
                Raw_data[i][targetIndex] = elems[sourceIndex].trim();
              } else {
                Raw_data[i][targetIndex] = "";
              }
              
            }
            
            Sequence += elems[1].trim();
            
          } catch (Exception e) {
            e.printStackTrace();
          }

        };//eof for i

        in.close();
        fr.close();


        Sequence.trim();

        csi_Se = new CSI_Sec (Raw_data);


        Remove_deviant_data (Raw_data);//fill No_deviant_data too, and also cal suspious data.

        csi_Se = new CSI_Sec (No_deviant_data);

        Find_s_data (csi_Se.get_ref_seqCSI());

        secstr_no_cal=csi_Se.get_Sec_No_Cal();
        secstr_with_cal=csi_Se.get_Sec_with_Cal();


      }catch (Exception e) {
      }

    };// eof file_type


 


    if (file_type.equals("shiftY")) {

      missed=false;

      boolean start = false;

      Vector <String> tmp = new Vector <String>();


      try {
        FileReader fr = new FileReader(strfile);
        BufferedReader in = new BufferedReader(fr);
        String line;

        int s_num=0;


        while ((line=in.readLine())!=null) {

          // System.out.println(line+"    first");


          if( line.trim().startsWith("NUM")|| line.trim().startsWith("*NUM ")||line.trim().startsWith("Num ") ) {
            // System.out.println(line +"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
            StringTokenizer st = new StringTokenizer(line.trim()) ;
            int i=0;
            while (st.hasMoreTokens()) {
              if (st.nextToken().trim().equals("HA")) {
                s_num=i;
                break;
              };
            i++;
            };
            start=true;
          }; //eof if 

          if(start && (line.trim().length()>5) && (!line.trim().startsWith("Num"))&& (!line.trim().startsWith("NUM"))  )  {
            // System.out.println(line);
            tmp.add(line.replace("=", ""));
            // System.out.println(line.replace("=", ""));
          };

        };// eof while

        Raw_data = new String [tmp.size()][9];

        for (int i=0; i<Raw_data.length; i++) {

          StringTokenizer st = new StringTokenizer(tmp.elementAt(i)) ;
          String[] a = new String[st.countTokens()];
          int j=0;
          while (st.hasMoreTokens()) { a[j]=st.nextToken().trim();j++;};

          try {
            Raw_data[i][0] = a[0].trim();
            Raw_data[i][1] = a[1].trim();
            if(i>0)
              Raw_data[i][2] = Raw_data[i-1][1]; 
            else
              Raw_data[i][2] ="";

              // Raw_data;   num, res, pre-res,  "C",  "CA",  "CB",   "N",   "H",  "HA"  raw csi data 10_18_2009
                   //          0    1      2      3      4      5      6       7    8 



            try {
              if (Double.parseDouble(a[s_num+3])>1.0)
                Raw_data[i][3] = a[s_num+3].trim();
              else
                Raw_data[i][3]="";
            } catch (Exception e7) {
            };
            try {
              if (Double.parseDouble(a[s_num+1])>1.0)
                Raw_data[i][4] = a[s_num+1].trim(); 
              else
               Raw_data[i][4]="";
            } catch (Exception e5) {
            };
            try {
              if (Double.parseDouble(a[s_num+2])>1.0)
                Raw_data[i][5] = a[s_num+2].trim();
              else
                Raw_data[i][5]="";
            } catch (Exception e6) {
            };
            try {
              if (Double.parseDouble(a[s_num+4])>1.0)
                Raw_data[i][6] = a[s_num+4].trim(); 
              else
                Raw_data[i][6]="";
            } catch (Exception e4) {
            };
            try {
              if (Double.parseDouble(a[s_num+5])>1.0)
                Raw_data[i][7] = a[s_num+5].trim();
              else
                Raw_data[i][7]="";
            } catch (Exception e3) {
            };
            try {
              if (Double.parseDouble(a[s_num])>1.0)
                Raw_data[i][8] = a[s_num].trim(); 
              else
                Raw_data[i][8]="";
            } catch (Exception e2) {
            };


            Sequence+=a[1].trim();

          } catch (Exception ed) {
          }

        };//eof for i


        in.close();
        fr.close();


        Sequence.trim();

        csi_Se = new CSI_Sec (Raw_data);


        Remove_deviant_data (Raw_data);//fill No_deviant_data too, and also cal suspious data.

        csi_Se = new CSI_Sec (No_deviant_data);

        Find_s_data (csi_Se.get_ref_seqCSI());

        secstr_no_cal=csi_Se.get_Sec_No_Cal();
        secstr_with_cal=csi_Se.get_Sec_with_Cal();



      } catch (Exception eee) {
      }


    };// eof file_type



  } // eof constructor

  //----

  public String getSequence() {
    return Sequence;
  }


  protected void Fill_csi_table (Vector data, int[] idex, String file_type) {
    
    Raw_data = new String[Sequence.length()][9];
    for (int l=0; l<Raw_data.length; l++) {
      for (int k=0; k<Raw_data[0].length; k++)
        Raw_data[l][k]="";
    };

    for (int m=0; m<Sequence.length(); m++) {// further cali need for negative starting num in assig

      Raw_data[m][0]=Integer.toString(m+1);   
      Raw_data[m][1]=Sequence.substring(m,m+1); // String consisting of char at index m
      if (m>1)
        Raw_data[m][2]=Sequence.substring(m-1,m); //pre-residue             
    };

    if (TESTCSPARSER) {
      System.out.println("Raw_data before processing chem shifts table:");
      for (int l=0; l<Raw_data.length; l++) {
        for (int k=0; k<Raw_data[0].length; k++) {
          System.out.print(Raw_data[l][k]);
          if (k != Raw_data[0].length - 1) {
            System.out.print("\t");
          }
        }
        System.out.println();
      }
    }

    String firstChainCode = null;
    for (int i=0; i<data.size(); i++) { // iterate over lines from chem shift table

      StringTokenizer token = new StringTokenizer((String)data.elementAt(i), " ");

      String[] line = new String[token.countTokens()];
      for (int a=0;a<line.length;a++)  
        line[a] = token.nextToken(); 

      try {  

        // Set lookup variables, which give the index in idex[] for various column types.
        // The index values into idex[] correspond to the indices of the labels in 
        // bmrbVersion2Labels, bmrbVersion3Labels, or nefLabels.
        int seqNumLookup; // For integer that numbers residues sequentially starting from 1.
        int chainCodeLookup;
        int seqCodeLookup; // For string that identifies residues.
        int resCodeLookup;
        int atomCodeLookup;
        int csValLookup;
        if (file_type.equals("str")) {
          seqNumLookup = BMRB_SEQ_NUM_LOOKUP;
          chainCodeLookup = -1;
          seqCodeLookup = -1;
          resCodeLookup = BMRB_RES_LABEL_LOOKUP;
          atomCodeLookup = BMRB_ATOM_NAME_LOOKUP;
          csValLookup = BMRB_CS_VAL_LOOKUP;
        } else { // file_type.equals("nef")
          seqNumLookup = -1;
          chainCodeLookup = NEF_CS_TBL_CHAIN_CODE_LOOKUP;
          seqCodeLookup = NEF_CS_TBL_SEQ_CODE_LOOKUP;
          resCodeLookup = NEF_CS_TBL_RES_LABEL_LOOKUP;
          atomCodeLookup = NEF_CS_TBL_ATOM_NAME_LOOKUP;
          csValLookup = NEF_CS_TBL_CS_VAL_LOOKUP;
        }

        int Seq_num; // Integer that numbers residues sequentially starting from 1.
        if (file_type.equals("str")) {
          Seq_num = Integer.parseInt(line[idex[seqNumLookup]]);
        } else { // file_type.equals("nef")
          
          String chainCode = line[idex[chainCodeLookup]];
          String seqCode = line[idex[seqCodeLookup]];
          
          if (Pattern.matches(".*\\@.*", seqCode)) {
            // Unassigned resonance. Skip it.
            continue;
          }
      
          if (firstChainCode == null) {
            firstChainCode = chainCode;
          }

          if (!chainCode.equals(firstChainCode)) {
            // We only want to parse the first chain in the chem shift table.
            continue;
          }
          
          Seq_num = this.getResInteger(chainCode, seqCode, idex);
        }

        String Res_type = three_to_one(line[idex[resCodeLookup]].trim());
          //System.out.println("Res_type: " + Res_type);

        int Atom_place = base.atomToInt(line[idex[atomCodeLookup]]);//{"C","CA","CB","N","H","HA"}
        //System.out.println("  Col index: " + idex[atomCodeLookup]);
        //System.out.println("  Atom code: " + line[idex[atomCodeLookup]]);
        //System.out.println("  Int: " + Atom_place);

        String csi =line[idex[csValLookup]]; // chem shift value
          //System.out.println("csi: " + csi);


        boolean match = false;

        if (Res_type.equals(Sequence.substring(Seq_num-1,Seq_num)))
          match=true;

    
        if(Atom_place!=-1) 
          Raw_data[Seq_num-1][Atom_place+3] = csi;//below for Gly HA shifts



        if (Res_type.equals("G") &&  (line[idex[atomCodeLookup]].equals("HA2") || line[idex[atomCodeLookup]].equals("HAX")))
          Raw_data[Seq_num-1][8] = line[idex[csValLookup]]; // get chem shift value

        if (Res_type.equals("G") &&  (line[idex[atomCodeLookup]].equals("HA3") || line[idex[atomCodeLookup]].equals("HAY"))) {
          // try to use the average of HA2 and HA3 (or HAX and HAY) chem shifts
          try {
            //String ave_Gly_HA = nf.format (   (  Double.parseDouble(Raw_data[Seq_num-1][8])
            //                                                + Double.parseDouble(line[idex[csValLookup]]) )/2.0);
            String ave_Gly_HA = Double.toString (   (  Double.parseDouble(Raw_data[Seq_num-1][8])
                                                            + Double.parseDouble(line[idex[csValLookup]]) )/2.0);

            Raw_data[Seq_num-1][8] = ave_Gly_HA ;   
          } catch(Exception gly) {}
        };

      } catch(Exception add) {
        add.printStackTrace();
      }



    }//eof i data 

    if (TESTCSPARSER) {
      System.out.println("Raw_data after filling chem shifts table:");
      for (int l=0; l<Raw_data.length; l++) {
        for (int k=0; k<Raw_data[0].length; k++) {
          System.out.print(Raw_data[l][k]);
          if (k != Raw_data[0].length - 1) {
            System.out.print("\t");
          }
        }
        System.out.println();
      }
    }

  };//eof Fill_csi



  /*
   * For NEF format. Given the chain code and sequence code (i.e. a string that is not
   * necessarily an integer), this method returns an integer that uniquely identifies the
   * residue, with numbering starting from 1 for the first residue and proceeding
   * sequentially.
   */
  private int getResInteger(String chainCodeToFind, String seqCodeToFind, int[] idex) {
    
    for (int i = 0; i < molecularSystemLines.size(); i++) {
      String line = molecularSystemLines.elementAt(i);
      
      Matcher m = Pattern.compile("^\\s*").matcher(line);
      line = m.replaceFirst("");
      
      String[] elems = line.split("\\s+");
      
      String seqCode = elems[idex[NEF_MOL_SYS_SEQ_CODE_LOOKUP]];
      
      if (!seqCode.equals(seqCodeToFind)) {
        continue;
      }
      
      String chainCode = elems[idex[NEF_MOL_SYS_CHAIN_CODE_LOOKUP]];
      
      if (chainCode.equals(chainCodeToFind)) {
        return i + 1;
      }
    }
    
    System.err.println("ERROR: Residue " + chainCodeToFind + seqCodeToFind + " from chem shifts table not found in sequence table.\n");
    System.exit(1);
    return -1;
  }










  /*...............................................*/	

  protected void Remove_deviant_data (String [][] s_CSI) {



    No_deviant_data = new String[s_CSI.length][s_CSI[0].length];

    double[] ave =  {175.7, 56.6, 34.4, 119.3, 7.93, 4.41};// no G for CA, no A S T for CB estimated ave

    double[] sum = {0.0,0.0,0.0,0.0,0.0,0.0}; // {"C","CA","CB","N","H","HA"};
    double[] num = {0.0,0.0,0.0,0.0,0.0,0.0}; 

    // modified 2/6/2010, not for HN and HA since there is now refer problem, no need 

    for (int e=0;e< No_deviant_data.length; e++) {//get averaged 

      String aa = s_CSI[e][1].trim();
      int aaIndex=AA.indexOf(aa);
      for (int j=0; j<6; j++) {
        try {double csi = Double.parseDouble(s_CSI[e][j+3]); 
        
          if (csi>(d_range[j][aaIndex][1]) || csi<(d_range[j][aaIndex][0]) ){
            //System.out.println("-----------"+j+" : "+e+": "+":"+ aaIndex+":"+aa+" : "+csi + " : "+ d_range[j][aaIndex][0]+":"+d_range[j][aaIndex][1]);
          }
          else {
            if ((!(j==1 && aa.equals("G"))) && (!(j==2 && (aa.equals("A") ||aa.equals("S")||aa.equals("T"))))) {
               sum[j]+= csi;
               num[j]+=1.0; 
             }
          }
        } catch(Exception dd){
        }
      };//eof j

    };//eof e

    for (int m=0; m<ave.length; m++) {
      try{
        double tmp =sum[m]/num[m]-ave[m];
        ave[m]=tmp;
      } catch(Exception de) {
      }
    };


    for (int i=0; i<No_deviant_data.length; i++) {
      int a=AA.indexOf(s_CSI[i][1]);

      for (int j=0; j<No_deviant_data[0].length; j++) {

        if (j<3)
          No_deviant_data[i][j]=s_CSI[i][j];
        else if (j>2 && j<7) {
          try {
            double csi = Double.parseDouble(s_CSI[i][j]);
            Total_CSI++;

            if (csi>(d_range[j-3][a][0]+ ave[j-3]) && csi< (d_range[j-3][a][1]+ave[j-3]) ) {
              No_deviant_data[i][j]=s_CSI[i][j];

            }
            else {
              Total_D++;

              D_data += s_CSI[i][1]+s_CSI[i][0]+"  "+ Atoms[j-3] +": "+ s_CSI[i][j]+"\n ";


            };
          } catch(Exception ed) {
            No_deviant_data[i][j]="";
          }

        } else if (j>6) {
          try {
            double csi = Double.parseDouble(s_CSI[i][j]);
            Total_CSI++;

            if ( csi>d_range[j-3][a][0] && csi<d_range[j-3][a][1] ) {
                No_deviant_data[i][j]=s_CSI[i][j];

            }
            else {
              Total_D++;

              D_data += s_CSI[i][1]+s_CSI[i][0]+"  "+ Atoms[j-3] +": "+ s_CSI[i][j]+"\n ";


            };
          } catch(Exception ed){
            No_deviant_data[i][j]="";
          }

        }




      }; //eof j
    };//eof for i

  };





  protected void Find_s_data (String [][] xxx) {

    for (int i=0; i<xxx.length; i++) {
      int a=AA.indexOf(xxx[i][1]);

      for (int j=0; j<xxx[0].length; j++) {

        if (j>2) {
          try {
            double csi = Double.parseDouble(xxx[i][j]);
            if (csi<(s_range[j-3][a][0]) || csi> (s_range[j-3][a][1]) ) {
              Total_S++;
              S_data += xxx[i][1]+xxx[i][0]+"  "+ Atoms[j-3] +": "+ xxx[i][j]+"\n ";       
            };    
          } catch(Exception edd){
          }
        }
      }; //eof j
    };//eof for i

  };


  /*...............................................*/	



  // Returns display data for main chem shifts table.
  public Object[][] getDisplayElements() {

    Object[][] displayElements = new Object[Raw_data.length][11];
    for(int a=0;a<displayElements.length;a++) {



      for(int b=0;b<displayElements[0].length;b++) {
        if(b<1) { // Seq column
          displayElements[a][b]=Raw_data[a][b+1]+Raw_data[a][b];
        }
        else if (b<7) { // Chem shift columns
          try {
            if (Pattern.matches(".*?\\d*\\.\\d{4}\\d.*", Raw_data[a][b+2])) {
              displayElements[a][b] = df.format(Double.parseDouble(Raw_data[a][b+2]));
            } else {
              displayElements[a][b]=Raw_data[a][b+2];
            }
          } catch (Exception e) {
            displayElements[a][b]=Raw_data[a][b+2];
          }
        }
        else  { // Secondary structure columns
          displayElements[a][7]=secstr_no_cal[a][1];
          displayElements[a][8]=secstr_no_cal[a][2];
          displayElements[a][9]=secstr_no_cal[a][3];
          displayElements[a][10]=secstr_no_cal[a][4];
        }

      }

    }

    return displayElements;
  }


  // Returns display data for calibrated chem shifts table.
  public String[][] getCal_DisplayElements () {

    need_ref_cal=csi_Se.need_cal();


    String[][] No_Abnormal_RefCal_seqCSI= csi_Se.get_ref_seqCSI();

    String[][] CalData = new String[No_Abnormal_RefCal_seqCSI.length][11];
    for(int a=0;a<CalData.length;a++) {
    
      // Seq column
      CalData[a][0]=No_Abnormal_RefCal_seqCSI[a][1]+No_Abnormal_RefCal_seqCSI[a][0];

      // Chemical shift columns
      for (int col = 1; col <= 6; col++) {
        try {
          
          if (Pattern.matches(".*?\\d*\\.\\d{4}\\d.*", No_Abnormal_RefCal_seqCSI[a][col+2])) {
              CalData[a][col] = df.format(Double.parseDouble(No_Abnormal_RefCal_seqCSI[a][col+2]));
          } else {
            CalData[a][col]=No_Abnormal_RefCal_seqCSI[a][col+2];
          }
          
        } catch (Exception e) {
          CalData[a][col]=No_Abnormal_RefCal_seqCSI[a][col+2];
        }
      }
      
      // Secondary structure columns
      CalData[a][7]=secstr_with_cal[a][1];//
      CalData[a][8]=secstr_with_cal[a][2];//
      CalData[a][9]=secstr_with_cal[a][3];//
      CalData[a][10]=secstr_with_cal[a][4];//
    }

    ref_shift=csi_Se.get_ref_calib ();
    return CalData;

  }



  public String[][] get_SequenceLine() {
    String [][] tmp = new String[Raw_data.length][2];

    for (int a=0;a<tmp.length;a++) {
      tmp[a][0]=Raw_data[a][1];
      tmp[a][1] = Integer.toString(base.aminoToInt(tmp[a][0]));
    }
    return tmp;
  };

  public int[] getSequenceLine () {
    int[] tmp = new int [Sequence.length()];

    for (int a=0;a<tmp.length;a++) {

      tmp[a] = base.aminoToInt(Sequence.substring(a, a+1));
    }
    return tmp;
  }



  public String  get_D_S_shift () {
    String tmp="Number of assignments: "+ Integer.toString(Total_CSI)+ "\n";
    tmp+= "Number of deviant assignments: "+Integer.toString(Total_D) +" \n";
    tmp+=D_data+"\n";

    tmp+="Number of suspicious assignments after ref-calibration when necessary: "+ 
    Integer.toString(Total_S) +" \n";
    tmp += S_data+"\n";

    return tmp;
  };



  public String  get_D_S_shift2 () {
    String tmp=(Integer.toString(Total_D)+"     ");
    tmp+=(Integer.toString(Total_S)+"     ");

    return tmp;
  };






  // para 1 sequence of data,para 2, prob of 20 amino (chose from 3 sec, highest)

  public Double[][] getSixty() {
    return base.get_seq_amino_Prob (No_deviant_data);
  }


  public Double[][] getSixtyCal() {

    String[][] No_Abnormal_RefCal_seqCSI= csi_Se.get_ref_seqCSI();
    return base.get_seq_amino_Prob (No_Abnormal_RefCal_seqCSI);
  }


  public boolean[] getscannableList (int modlength) {

    int scannable=0;
    boolean[] boo = new boolean[Raw_data.length-modlength+1];
    double present = 0.0;
    double percentage = 0.0;
    for(int i=0;i<boo.length;i++) {
      present = 0.0;
      for(int j=0;j<modlength;j++){
        try{
          Double.parseDouble(Raw_data[i+j][4]);
          present++;
        }catch(Exception e){}
        try{
          Double.parseDouble(Raw_data[i+j][5]);
          present++;
        }catch(Exception e){}
      }
        percentage = present/(modlength*2);
        if(percentage<.5)
        boo[i] = false;
      else{
        boo[i] = true;
        scannable++;
      }

    }

    return boo;
  }

  public boolean need_cal () {
    return csi_Se.need_cal();
  };

  public String get_ref_cal () {
    return csi_Se.get_ref_calib();
  };


}
