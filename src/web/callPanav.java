package web;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;

import com.oreilly.servlet.MultipartRequest;


@WebServlet(name="callPanavServlet",urlPatterns={"/callPanav"})
public class callPanav extends HttpServlet{
	
	File input;
	String input_type;
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException 
	{
		doPost(req,res);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		//String dest="/PANAV";
		String dest="/";
		try {
			String output_dir = getServletContext().getRealPath("/") + "/tmp/";
			MultipartRequest multi = new MultipartRequest(req, output_dir,100 * 1024 * 1024);
			File file = multi.getFile("file");
			
			Date today=new Date();
	    	String output_name=today.getTime()+".htm"; //today.getTime()+".out";
	    	String output=output_dir+output_name;
	    	String type="Invalid input format";
	    	panavWeb pweb=new panavWeb(file);
	    	if (pweb.isValidated(type)) {
	    		pweb.process(pweb.input_type,file.getAbsolutePath(),output);
	    		System.out.println("suc="+pweb.input_type);
	    		dest=dest+"tmp/"+output_name;
	    	}
	    	else {
	    		System.out.println(type);
	    	}
	    	res.sendRedirect(res.encodeRedirectURL(dest));
		}
		catch (Exception ex) {
			res.sendRedirect(res.encodeRedirectURL(dest));
			ex.printStackTrace();
		}

	}
	
	
}
