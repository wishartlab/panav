package web;
import java.io.*;
import java.util.Date;
import java.util.Hashtable;
import java.util.regex.*;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.oreilly.servlet.MultipartRequest;

import standalone.Scanner_3;
import standalone.chemicalShift;
import standalone.CSFileReader;

public class panavWeb extends HttpServlet {

	File input;
	String input_type;
	public panavWeb(File f) {
		this.input=f;
	}
	
	public boolean isValidated(String msg) {
		
		boolean valid=false;
		try {
			BufferedReader in = new BufferedReader(new FileReader(input));
			String line;
			int count=0,allc=0,tot=0,entryc=0;
			Hashtable hsh=new Hashtable();
			
			boolean firstLine = true;
			while ((line=in.readLine())!=null) {
				//System.out.println(line);
				if (firstLine && Pattern.matches("^\\s*NUM\\s+RES\\b.*", line)) {
					valid=true;
					input_type="shiftX_table";
					break;
				}
				else if (line.contains("Query Seq:")) {
					valid=true;
					input_type="shiftY";
					break;
				}
				else if (line.contains("Entry information")) {
					valid=true;
					input_type="str";
					break;
				}
				else if (Pattern.matches("\\s*_nef_nmr_meta_data\\.format_name\\s*nmr_exchange_format\\s*", line)) {
					valid=true;
					input_type="nef";
					break;
				}
				
				firstLine = false;
			}
			//if can't verify by content, try to verify by filename extension
			if (!valid) {
				String fname=input.getName();
				if (fname.contains("shiftX")) {	
					input_type="shiftX_table";
				}
				else if (line.contains("shifty")) {	
					input_type="shiftY";
				
				}
				else if (line.contains(".str")) {					
					input_type="str";				
				}
				
			}
			System.out.println("file_type="+input_type);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			
		}
		return valid;
	}
	
	public String getCSIStruct(chemicalShift chemical, String titleColor) {
		StringBuffer str=new StringBuffer("<table border='1' cellspacing='2' width='700'>\n");
		str.append("<tr><td bgcolor=titleColor>Seq</td><td bgcolor=titleColor>C</td><td bgcolor=titleColor>CA</td><td bgcolor=titleColor>CB</td><td bgcolor=titleColor>N</td><td bgcolor=titleColor>H</td><td bgcolor=titleColor>HA</td><td bgcolor=titleColor>Beta sheet probability</td><td bgcolor=titleColor>Coil probability</td><td bgcolor=titleColor>Helix probability</td><td bgcolor=titleColor>SecStr</td><tr>");
		 
		for (int i=0;i < chemical.getRowCount();i++) {
			String line="<TR>\n";
			for (int j=0;j<chemical.getColumnCount();j++) {
				String value=(String)chemical.getValueAt(i, j);
				if (value==null) {
					value="";
				}
				else
					value=value.trim();
				value="<td>"+value+"&nbsp;</TD>";
				line=line+value;
			}
			line=line+"</TR>\n";
			str.append(line);
		}
		str.append("</table>");
		return str.toString();
	}
	
	public String [] parseOutput(String output) {
		StringBuffer buf=new StringBuffer();
		String out[]={"",""};
		try {
			BufferedReader in = new BufferedReader(new FileReader(output));
			String line;
			int count=0,allc=0,tot=0,entryc=0;
			Hashtable hsh=new Hashtable();
			//System.out.println("out="+output);
			boolean print=false;
			while ((line=in.readLine())!=null) {
				//System.out.println(line);
				if (line.contains("3-Resdue")) {
					print=true;
				}
				else if (line.contains("Detected reference offsets")) {
					out[0]=buf.toString();
					buf=new StringBuffer();
				}
				if (print) {
					buf.append(line);
					buf.append("<br>");
				}
				
			}
			out[1]=buf.toString();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return out;
	}
	
	public String getValidation(CSFileReader strreader) {
		
		String dmesg = "";

        String seq = strreader.getSequence();
        int[] aaIndex = strreader.getSequenceLine();
        Double[][] sixtycal = strreader.getSixtyCal();

        for (int r = PANAV.RES_LEN_MIN; r <= PANAV.RES_LEN_MAX; r++) {

          boolean[] CaCb_sel = strreader.getscannableList(r);
          Scanner_3 scan = new Scanner_3(seq, aaIndex, sixtycal, r,
                                         PANAV.allow, CaCb_sel);
          int[] s_position = scan.get_s_pos();
          String[] score = scan.get_Score();
          String[] s_nor_prob = scan.get_o_prob_nor();
         dmesg+= "\n" + r + "-Resdue Scan: \n\n";
         boolean[] x = new boolean[s_position.length + r];
         for (int q = 0; q < x.length; q++) {
              x[q] = true;
         }

        for (int q = 0; q < x.length - r; q++) {
              for (int m = 0; m < r; m++) {
                if (s_position[q] > 0) {
                  x[q + m] = false;
                }
            }
        }
       for (int i = 0; i < x.length-1; i++) {
                if (x[i]) {
                  dmesg+= seq.substring(i, i + 1);
                }
                else {
                 // dmesg+=seq.substring(i, i + 1);
                  dmesg+="<font color='red'>"+seq.substring(i, i + 1)+"</font>"; //If want this part stands out(bold)
                }

                if (Integer.toString(i + 1).endsWith("0") && i > 0) {
                  dmesg+= "  ";
                }
      }
      dmesg+="\n\nOriginal Assigment and Probability vs Suggested Assignment and Probability \n\n";
      dmesg+=scan.get_message()+"\n";
      dmesg+= "# of selected frags: " + score[0] + ";   # of confirmed frags: " +
             score[1] +"; CONA Score:  " + score[2] + "\n";
        }
        dmesg=dmesg.substring(1,dmesg.length()).replaceAll("\n", "<br>");
		return dmesg;
	}
	
	public String getOffset (CSFileReader strreader) {
		String msg="";
		if (strreader.need_ref_cal) {
			msg="Detected reference offsets \n" + strreader.ref_shift +
	                         "\n\n" + strreader.get_D_S_shift();
	    }
	    else {
	    	msg="No reference offsets required" + "\n\n" +
	                         strreader.get_D_S_shift();
	    }
		msg=msg.replaceAll("\n", "<br>");
		return msg;
	}
	
	public String process(String type, String input, String output) {
		
		String out="";
		String inputName=this.input.getName();
		try {
			//PANVA pan=new PANVA(type,input,output);
			//String outs[]=parseOutput(output);  //deprecated; changed Yunjun's code so that i'm acessing the functions directly instead of parsing his output
			//System.out.println(outs[0]);
			//System.out.println("fdsf="+outs[1]);
			PrintWriter outwriter   = new PrintWriter(new BufferedWriter(new FileWriter(output)));     
			outwriter.println(getHTMLHeader("Processed File: "+ inputName ));
			
			
			CSFileReader strreader=new CSFileReader(new File (input), type);
			
			chemicalShift chemical = new chemicalShift(strreader.getDisplayElements());
			chemicalShift calibrate = new chemicalShift(strreader.getCal_DisplayElements());
			String csi="CSI and Sec Structure ("+inputName+")";
			outwriter.println(addHTMLBody(getCSIStruct(chemical, "#00FF00"),csi));
			csi="CSI and Sec Structure (No_Deviant_Ref_Calibrated)";
			outwriter.println(addHTMLBody(getCSIStruct(calibrate, "#FF0000"),csi));
			
			outwriter.println(addHTMLBody(this.getValidation(strreader), "Validation"));
			outwriter.println(addHTMLBody(getOffset(strreader), "Reference Offsets and Deviant Shifts"));
			//System.out.println(finput);
			
			outwriter.println(getHTMLFooter());
			outwriter.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return out;
	}
	
	public String getHTMLHeader(String title) {
		String header="<HTML><HEADER><TITLE>"+title+"</TITLE></HEADER><BODY>";
		return header;
	}
	
	public String getHTMLFooter() {
		String header="</BODY></HTML>";
		return header;
	}
	
	public String addHTMLBody(String section, String sectionTitle) {
		String title="<B>"+sectionTitle+"</B>";
		StringBuffer segment=new StringBuffer(title);	
		segment.append("<P>").append(section).append("</P>");	
		return segment.toString();
	}
	

	
    public static void main(String[] args) {
    	
    	//String input=args[0];
    	//String input="F:/WORK/PANAV/resourcecode/2TRX_shiftX_table.txt";
    	String input="F:/WORK/PANAV/resourcecode/4790_shifty.txt";
    	//String input="F:/WORK/PANAV/resourcecode/bmr10002.str";
    	panavWeb pweb=new panavWeb(new File(input));
    	String output_dir="C:/tmp/";
    	String output_name="output.htm"; //today.getTime()+".out";
    	String output=output_dir+output_name;
    	String type="Invalid input format";
    	if (pweb.isValidated(type)) {
    		pweb.process(pweb.input_type,input,output);
    		System.out.println("suc="+pweb.input_type);
    	}
    	else {
    		System.out.println(type);
    	}
    }

}
