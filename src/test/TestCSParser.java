package test;

import standalone.CSFileReader;
import java.io.*;

public class TestCSParser {

	public static void main(String[] args) {
	  
	  if (args.length != 2) {
	    System.out.println("Usage: java -jar build/TestCSParser.jar [CS file] [format]");
	    System.out.println("   Where [CS file] is a chemical shifts file to test,");
	    System.out.println("         [format] specifies the file format:");
	    System.out.println("           str, nef, shiftX_table, or shiftY");
	    System.exit(0);
	  }
	  
	  String filename = args[0];
	  String format = args[1];
	  
	  CSFileReader.TESTCSPARSER = true;
		CSFileReader r = new CSFileReader(new File(filename), format);
		System.out.println("Done test.");
	}
	
}
